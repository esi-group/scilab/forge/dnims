/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
template <typename T, typename U,class V>
int wrapSingleWithCast(T x, int envId)
{
    return V::wrap(static_cast<U>(x), envId);
}
/*--------------------------------------------------------------------------*/
template <typename T, typename U, class V>
int wrapRowWithCast(T *x, int len, int envId)
{
    int i;
    U *l = new U[len];
    for (i = 0; i < len ; i++)
    {
        l[i] = static_cast<U>(x[i]);
    }

    return V::wrap(l, len, envId);
}
/*--------------------------------------------------------------------------*/
template <typename T, typename U, class V>
int wrapMatWithCast(T *x, int r, int c, int envId)
{
    if (getMethodOfConv())
    {
        U **xx = new U*[r];
        int i, j;
        for (i = 0; i < r; i++)
        {
            xx[i] = new U[c];
            for (j = 0; j < c; j++)
            {
                xx[i][j] = static_cast<U>(x[j * r + i]);
            }
        }
        j = V::wrap(xx, r, c, envId);
        for (i = 0; i < r; delete [] xx[i++]);
        delete [] xx;
        return j;
    }
    else
    {
        U **xx = new U*[c];
        int i, j;
        for (i = 0; i < c; i++)
        {
            xx[i] = new U[r];
            for (j = 0; j < r; j++)
            {
                xx[i][j] = static_cast<U>(x[i * r + j]);
            }
        }
        j = V::wrap(xx, c, r, envId);
        for (i = 0; i < c; delete [] xx[i++]);
        delete [] xx;
        return j;
    }
}
/*--------------------------------------------------------------------------*/
