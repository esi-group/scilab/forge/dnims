/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "noMoreMemory.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/* Function invoke is called with more than 2 arguments : invoke(envid, objid, method, arg1,...,argn)
   - obj is a _JObj mlist so we get his id or can be a String or a number. In this last case, the obj is converted
   in a Java object and the id is got.
   - method is the name of the method.
   - arg1,...,argn are the arguments of the called method, if they're not _JObj mlist, they're converted when it is possible
*/

int sci_invoke_lu(char *fname)
{
    SciErr err;
    int typ = 0;
    int *addr = NULL, *listaddr = NULL;
    int len = 0;
    int *tmpvar = NULL;
    int id = 0;
    int *args = NULL;
    int *child = NULL;
    char *methName = NULL;
    int i = 0;
    char *errmsg = NULL;
    int ret = 0;
	int envId = -1;

    CheckRhs(4, 4);

    setCopyOccured(0);
    initialization(NULL);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 4, &listaddr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    err = getVarType(pvApiCtx, listaddr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (typ != sci_list)
    {
        Scierror(999, "%s: Wrong type for input argument %i : List expected\n", fname, 4);
        return NULL;
    }

    err = getListItemNumber(pvApiCtx, listaddr, &len);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    tmpvar = (int*)MALLOC(sizeof(int) * (len + 1));
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }
    *tmpvar = 0;

	err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
	if (err.iErr)
	{
		FREE(tmpvar);
		printError(&err, 0);
		return 0;
	}

	if (getScalarInteger32(pvApiCtx, addr, &envId))
	{
		FREE(tmpvar);
		return 0;
	}

    err = getVarAddressFromPosition(pvApiCtx, 2, &addr);
    if (err.iErr)
    {
        FREE(tmpvar);
        printError(&err, 0);
        return 0;
    }

    if (getScalarInteger32(pvApiCtx, addr, &id))
    {
        FREE(tmpvar);
        return 0;
    }

    args = (int*)MALLOC(sizeof(int) * len);
    if (!args)
    {
        FREE(tmpvar);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }

    for (; i < len; i++)
    {
        err = getListItemAddress(pvApiCtx, listaddr, i + 1, &child);
        if (err.iErr)
        {
            FREE(args);
            removeTemporaryVarsEnv(envId, tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }
        args[i] = getIdOfArgEnv(child, fname, tmpvar, 0, i + 1, envId);
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            FREE(args);
            removeTemporaryVarsEnv(envId, tmpvar);
            FREE(tmpvar);
            return 0;
        }
    }

    methName = getSingleString(3, fname);
    if (!methName)
    {
        removeTemporaryVarsEnv(envId, tmpvar);
        FREE(tmpvar);
        FREE(args);
        return 0;
    }

    ret = invokeenv(envId, id, methName, args, len, &errmsg);
    freeAllocatedSingleString(methName);
    FREE(args);

    removeTemporaryVarsEnv(envId, tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!unwrapenv(ret, Rhs + 1, envId))
    {
        if (!createEnvironmentObjectAtPosEnv(_ENVOBJ, Rhs + 1, ret, envId))
        {
            removescilabobjectenv(envId, ret);
            return 0;
        }
    }
    else
    {
        removescilabobjectenv(envId, ret);
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
