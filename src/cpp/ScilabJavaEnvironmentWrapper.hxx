#ifndef __SCILABJAVAENVIRONMENTWRAPPER_H__
#define __SCILABJAVAENVIRONMENTWRAPPER_H__

#include "ScilabAbstractEnvironmentWrapper.hxx"

class ScilabJavaEnvironmentWrapper : public ScilabAbstractEnvironmentWrapper
{
public:
	virtual int wrapDouble(double x);

	virtual int wrapDouble(double* x, int xSize);

	virtual int wrapDouble(double** x, int xSize, int xSizeCol);

	virtual int wrapInt(int x);

	virtual int wrapInt(int* x, int xSize);

	virtual int wrapInt(int** x, int xSize, int xSizeCol);

	virtual int wrapUInt(long long x);

	virtual int wrapUInt(long long* x, int xSize);

	virtual int wrapUInt(long long** x, int xSize, int xSizeCol);

	virtual int wrapByte(byte x);

	virtual int wrapByte(byte* x, int xSize);

	virtual int wrapByte(byte** x, int xSize, int xSizeCol);

	virtual int wrapUByte(short x);

	virtual int wrapUByte(short* x, int xSize);

	virtual int wrapUByte(short** x, int xSize, int xSizeCol);

	virtual int wrapShort(short x);

	virtual int wrapShort(short* x, int xSize);

	virtual int wrapShort(short** x, int xSize, int xSizeCol);

	virtual int wrapUShort(int x);

	virtual int wrapUShort(int* x, int xSize);

	virtual int wrapUShort(int** x, int xSize, int xSizeCol);

	virtual int wrapString(char * x);

	virtual int wrapString(char ** x, int xSize);

	virtual int wrapString(char *** x, int xSize, int xSizeCol);

	virtual int wrapBoolean(bool x);

	virtual int wrapBoolean(bool* x, int xSize);

	virtual int wrapBoolean(bool** x, int xSize, int xSizeCol);

	virtual int wrapChar(unsigned short x);

	virtual int wrapChar(unsigned short* x, int xSize);

	virtual int wrapChar(unsigned short** x, int xSize, int xSizeCol);

	virtual int wrapFloat(float x);

	virtual int wrapFloat(float* x, int xSize);

	virtual int wrapFloat(float** x, int xSize, int xSizeCol);

	virtual int wrapLong(long long x);

	virtual int wrapLong(long long* x, int xSize);

	virtual int wrapLong(long long** x, int xSize, int xSizeCol);

	virtual void unwrapdouble(int, int);
	virtual void unwraprowdouble(int, int);
	virtual void unwrapmatdouble(int, int);

	virtual void unwrapbyte(int, int);
	virtual void unwraprowbyte(int, int);
	virtual void unwrapmatbyte(int, int);

	virtual void unwrapshort(int, int);
	virtual void unwraprowshort(int, int);
	virtual void unwrapmatshort(int, int);

	virtual void unwrapint(int, int);
	virtual void unwraprowint(int, int);
	virtual void unwrapmatint(int, int);

	virtual void unwrapboolean(int, int);
	virtual void unwraprowboolean(int, int);
	virtual void unwrapmatboolean(int, int);

	virtual void unwrapstring(int, int);
	virtual void unwraprowstring(int, int);
	virtual void unwrapmatstring(int, int);

	virtual void unwrapchar(int, int);
	virtual void unwraprowchar(int, int);
	virtual void unwrapmatchar(int, int);

	virtual void unwrapfloat(int, int);
	virtual void unwraprowfloat(int, int);
	virtual void unwrapmatfloat(int, int);

	virtual void unwraplong(int, int);
	virtual void unwraprowlong(int, int);
	virtual void unwrapmatlong(int, int);
};

#endif //__SCILABJAVAENVIRONMENTWRAPPER_H__