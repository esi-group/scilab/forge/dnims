/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_init(char *fname)
{
    char *errmsg = NULL;

    CheckRhs(0, 0);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    initializeenvironment(fname, &errmsg);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
    }
    else
    {
        PutLhsVar();
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
