/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_wrap(char *fname)
{
    SciErr err;
    int tmpvar[] = {0, 0};
    int *addr = NULL;
    int i = 1, id = 0;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    if (Rhs == 0)
    {
        Scierror(999,"%s: Wrong number of input arguments: 1 or more expected\n", fname);
        return 0;
    }

    CheckLhs(Rhs, Rhs);

    for (; i < Rhs + 1; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        id = getIdOfArg(addr, fname, tmpvar, 0, i);
        *tmpvar = 0;
        if (id == - 1)
        {
            if (i == 1)
            {
                OverLoad(1);
            }

            return 0;
        }
        if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + i, id, fname))
        {
            return 0;
        }

        LhsVar(i) = Rhs + i;
    }

    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
