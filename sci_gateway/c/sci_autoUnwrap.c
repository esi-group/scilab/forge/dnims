/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
/*--------------------------------------------------------------------------*/
int sci_autoUnwrap(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    int val = 0;
    int iType = 0;

    CheckRhs(0, 1);
    CheckLhs(1, 1);

    setCopyOccured(0);
    initialization(NULL);
    setIsNew(0);

    if (Rhs == 0)
    {
        createScalarBoolean(pvApiCtx, 1, (int)getAutoUnwrap());
        LhsVar(1) = 1;
        PutLhsVar();
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (iType != sci_boolean)
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: A boolean expected.\n"),fname,1);
        return 0;
    }

    if (!isScalar(pvApiCtx, piAddressVarOne))
    {
        Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
        return 0;
    }

    getScalarBoolean(pvApiCtx, piAddressVarOne, &val);
    setAutoUnwrap((const char)val);

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
