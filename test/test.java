import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

public class test
{
    public String field;

    public test(String str) {
	field = "Constructor has been called with String " + str;
    }

    public test(int t) {
	field = "Constructor has been called with int " + t;
    }

    public String getField() {
	return field;
    }

    public void setField(String s) {
	field = "Field has been set with " + s;
    }

    public static double F(double x) {
	return x;
    }
    
    public static double[] F(double[] x) {
	return x;
    }
    
    public static double[][] F(double[][] x) {
	return x;
    }
    
    public static int F(int x) {
	return x;
    }

    public static int[] F(int[] x) {
	return x;
    }

    public static int[][] F(int[][] x) {
	return x;
    }

    public static short F(short x) {
	return x;
    }

    public static short[] F(short[] x) {
	return x;
    }

    public static short[][] F(short[][] x) {
	return x;
    }

    public static byte F(byte x) {
	return x;
    }

    public static byte[] F(byte[] x) {
	return x;
    }

    public static byte[][] F(byte[][] x) {
	return x;
    }

    public static boolean F(boolean x) {
	return x;
    }

    public static boolean[] F(boolean[] x) {
	return x;
    }
    
    public static boolean[][] F(boolean[][] x) {
	return x;
    }

    public static String F(String x) {
	return x;
    }

    public static String[] F(String[] x) {
	return x;
    }

    public static String[][] F(String[][] x) {
	return x;
    }

    public static long F(long x) {
	return x;
    }

    public static long[] F(long[] x) {
	return x;
    }

    public static long[][] F(long[][] x) {
	return x;
    }

    public static char F(char x) {
	return x;
    }

    public static char[] F(char[] x) {
	return x;
    }

    public static char[][] F(char[][] x) {
	return x;
    }

    public static float F(float x) {
	return x;
    }

    public static float[] F(float[] x) {
	return x;
    }
    
    public static float[][] F(float[][] x) {
	return x;
    }

    public static void F(DoubleBuffer x) {
	for (int i = 0; i < x.capacity(); i++) {
	    x.put(i, x.get(i) + 1);
	}
    }

    public static void F(IntBuffer x) {
	for (int i = 0; i < x.capacity(); i++) {
	    x.put(i, x.get(i) + 1);
	}
    }

    public static void F(LongBuffer x) {
	for (int i = 0; i < x.capacity(); i++) {
	    x.put(i, x.get(i) + 1);
	}
    }

    public static void F(ShortBuffer x) {
	for (int i = 0; i < x.capacity(); i++) {
	    x.put(i, (short) (x.get(i) + 1));
	}
    }

    public static void F(ByteBuffer x) {
	for (int i = 0; i < x.capacity(); i++) {
	    x.put(i, (byte) (x.get(i) + 1));
	}
    }

    public static String returnUTF8String(String s) {
	return "Texte Java : \u00e0\u00e7\u00e8\u00ee\u00f6\u00f9" + " " + s;
    }
    
    public static double[][] transp(double[][] d) {
	double[][] td = new double[d[0].length][d.length];
	for (int i = 0; i < d.length; i++) {
	    for (int j = 0; j < d[0].length; j++) {
		td[j][i] = d[i][j];
	    }
	}
	return td;
    }
}
