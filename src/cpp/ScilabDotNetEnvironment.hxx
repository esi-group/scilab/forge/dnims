#ifndef __SCILABDOTNETENVIRONMENT_H__
#define __SCILABDOTNETENVIRONMENT_H__

#include "ScilabAbstractEnvironment.hxx"

class ScilabDotNetEnvironment: public ScilabAbstractEnvironment
{
	private:
		ScilabDotNetEnvironment();

	public:
		friend class ScilabAbstractEnvironmentFactory;

		virtual void garbagecollect();

		virtual int createarray(char *className, int* dims, int len);
				
		virtual int loadclass(char *className, char allowReload);

		virtual char* getrepresentation(int id);
				
		virtual int isvalidobject(int id);

		virtual int newinstance(int id, int *args, int argsSize);

		virtual int invoke(int id, char *methodName, int *args, int argsSize);

		virtual void setfield(int id, char *fieldName, int idarg);

		virtual int getfield(int id, char *fieldName);

		virtual int getfieldtype(int id, char *fieldName);

		virtual int getarrayelement(int id, int *index, int length);

		virtual void setarrayelement(int id, int *index, int length, int idArg);
				
		virtual int cast(int id, char *objName);
		
		virtual int castwithid(int id, int classId);
		
		virtual void removescilabobject(int id);
		
		virtual void getaccessiblemethods(int id, int pos);

		virtual void getaccessiblefields(int id, int pos);

		virtual char* getclassname(int id);

		virtual int isunwrappable(int id);

		virtual int compilecode(char *className, char **code, int size);

		//.NET-specific methods
		void load(const char* longName, bool allowReload);
		void loadfrom(const char* path, bool allowReload);
};

#endif //__SCILABDOTNETENVIRONMENT_H__
