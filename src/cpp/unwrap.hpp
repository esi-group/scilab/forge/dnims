/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/*
 * Template to handle several unwrapping operations
 */

#include <jni.h>
#include "api_scilab.h"
#include "GiwsException.hxx"
#include "NoMoreScilabMemoryException.hxx"
#include "JIMSwrapunwrap.h"

#define SCILABJAVAOBJECT "ScilabObjects/ScilabJavaObject"

template <typename T, typename U, class V>
void unwrapMat(JavaVM * jvm_, const bool methodOfConv, const int javaID, const int pos)
{
    SciErr err;
    jint lenRow, lenCol;
    jboolean isCopy = JNI_FALSE;
    jarray oneDim;
    JNIEnv * curEnv = NULL;
    U *addr = NULL;

    jvm_->AttachCurrentThread(reinterpret_cast<void **>(&curEnv), NULL);
    jclass cls = curEnv->FindClass(SCILABJAVAOBJECT);

    jmethodID id = curEnv->GetStaticMethodID(cls, V::getMatMethodName(), V::getMatMethodSignature()) ;
    if (id == NULL)
    {
        throw GiwsException::JniMethodNotFoundException(curEnv, V::getMatMethodName());
    }

    jobjectArray res = static_cast<jobjectArray>(curEnv->CallStaticObjectMethod(cls, id, javaID));
    if (curEnv->ExceptionCheck())
    {
        throw GiwsException::JniCallMethodException(curEnv);
    }

    lenRow = curEnv->GetArrayLength(res);
    oneDim = reinterpret_cast<jarray>(curEnv->GetObjectArrayElement(res, 0));
    lenCol = curEnv->GetArrayLength(oneDim);
    curEnv->DeleteLocalRef(oneDim);

    if (getMethodOfConv())
    {
        err = V::allocMatrix(pvApiCtx, pos, lenRow, lenCol, (void**) &addr);
    }
    else
    {
        err = V::allocMatrix(pvApiCtx, pos, lenCol, lenRow, (void**) &addr);
    }

    if (err.iErr)
    {
        curEnv->DeleteLocalRef(res);
        throw ScilabObjects::NoMoreScilabMemoryException();
    }

    T *resultsArray;
    for (int i = 0; i < lenRow; i++)
    {
        oneDim = reinterpret_cast<jarray>(curEnv->GetObjectArrayElement(res, i));
        resultsArray = static_cast<T *>(curEnv->GetPrimitiveArrayCritical(oneDim, &isCopy));
        if (getMethodOfConv())
        {
            for (int j = 0; j < lenCol; j++)
            {
                addr[j * lenRow + i] = static_cast<U>(resultsArray[j]);
            }
        }
        else
        {
            for (int j = 0; j < lenCol; j++)
            {
                addr[i * lenCol + j] = static_cast<U>(resultsArray[j]);
            }
        }
        curEnv->ReleasePrimitiveArrayCritical(oneDim, resultsArray, JNI_ABORT);
        curEnv->DeleteLocalRef(oneDim);
    }

    curEnv->DeleteLocalRef(res);
    if (curEnv->ExceptionCheck())
    {
        throw GiwsException::JniCallMethodException(curEnv);
    }
}

template <typename T, typename U, class V>
void unwrapRow(JavaVM * jvm_, const bool methodOfConv, const int javaID, const int pos)
{
    SciErr err;
    jint lenRow;
    jboolean isCopy = JNI_FALSE;
    JNIEnv * curEnv = NULL;
    U *addr = NULL;

    jvm_->AttachCurrentThread(reinterpret_cast<void **>(&curEnv), NULL);
    jclass cls = curEnv->FindClass(SCILABJAVAOBJECT);

    jmethodID id = curEnv->GetStaticMethodID(cls, V::getRowMethodName(), V::getRowMethodSignature());
    if (id == NULL)
    {
        throw GiwsException::JniMethodNotFoundException(curEnv, V::getRowMethodName());
    }

    jobjectArray res = static_cast<jobjectArray>(curEnv->CallStaticObjectMethod(cls, id, javaID));
    if (curEnv->ExceptionCheck())
    {
        curEnv->DeleteLocalRef(res);
        throw GiwsException::JniCallMethodException(curEnv);
    }

    lenRow = curEnv->GetArrayLength(res);
    err = V::allocMatrix(pvApiCtx, pos, 1, lenRow, (void**) &addr);

    if (err.iErr)
    {
        curEnv->DeleteLocalRef(res);
        throw ScilabObjects::NoMoreScilabMemoryException();
    }

    T *resultsArray = static_cast<T *>(curEnv->GetPrimitiveArrayCritical(res, &isCopy));
    for (int i = 0; i < lenRow; i++)
    {
        addr[i] = static_cast<U>(resultsArray[i]);
    }

    curEnv->ReleasePrimitiveArrayCritical(res, resultsArray, JNI_ABORT);
    curEnv->DeleteLocalRef(res);
    if (curEnv->ExceptionCheck())
    {
        throw GiwsException::JniCallMethodException(curEnv);
    }
}

template <typename T, typename U, class V>
void unwrapSingle(JavaVM * jvm_, const bool methodOfConv, const int javaID, const int pos)
{
    SciErr err;
    JNIEnv * curEnv = NULL;
    U *addr = NULL;

    jvm_->AttachCurrentThread(reinterpret_cast<void **>(&curEnv), NULL);
    jclass cls = curEnv->FindClass(SCILABJAVAOBJECT);

    jmethodID id = curEnv->GetStaticMethodID(cls, V::getMethodName(), V::getMethodSignature()) ;
    if (id == NULL)
    {
        throw GiwsException::JniMethodNotFoundException(curEnv, V::getMethodName());
    }

    err = V::allocMatrix(pvApiCtx, pos, 1, 1, (void**) &addr);

    if (err.iErr)
    {
        throw ScilabObjects::NoMoreScilabMemoryException();
    }

    *addr = static_cast<U>(V::getSingleVar(curEnv, cls, id, javaID));
    if (curEnv->ExceptionCheck())
    {
        throw GiwsException::JniCallMethodException(curEnv);
    }
}

/*
 * Structs defined to make easier the unwrapping
 */
#define __JIMS_wrapToUnwrapObject__(JType, Type, type, Signature) struct __JIMS__Scilab##JType##__ { \
        static SciErr allocMatrix(void* _pvCtx, int _iVar, int _iRows, int _iCols, void **_pdblReal) \
            {                                                           \
                return allocMatrixOf##Type(_pvCtx, _iVar, _iRows, _iCols, (type**) _pdblReal); \
            };                                                          \
        static type getSingleVar(JNIEnv * curEnv, jclass cls, jmethodID id, int javaID) \
            {                                                           \
                return static_cast<type>(curEnv->CallStatic##JType##Method(cls, id, javaID)); \
            };                                                          \
        __JIMS_getmethod__(,JType)                                      \
        __JIMS_getsignature__(,#Signature)                              \
        __JIMS_getmethod__(Row,JType)                                   \
        __JIMS_getsignature__(Row,"[" #Signature)                       \
        __JIMS_getmethod__(Mat,JType)                                   \
        __JIMS_getsignature__(Mat,"[[" #Signature)                      \
    };

__JIMS_wrapToUnwrapObject__(Double,Double,double,D)
__JIMS_wrapToUnwrapObject__(Boolean,Boolean,int,Z)
__JIMS_wrapToUnwrapObject__(Byte,Integer8,char,B)
__JIMS_wrapToUnwrapObject__(Short,Integer16,short,S)
__JIMS_wrapToUnwrapObject__(Char,UnsignedInteger16,unsigned short,C)
__JIMS_wrapToUnwrapObject__(Int,Integer32,int,I)
__JIMS_wrapToUnwrapObject__(Float,Double,double,F)
#ifdef __SCILAB_INT64__
__JIMS_wrapToUnwrapObject__(Long,Integer64,long,J)
#else
__JIMS_wrapToUnwrapObject__(Long,UnsignedInteger32,unsigned int,J)
#endif
