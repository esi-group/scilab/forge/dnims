#include "ScilabAbstractEnvironmentException.hxx"
#include "ScilabJavaEnvironmentWrapper.hxx"

#include "ScilabJavaObject.hxx"
#include "ScilabJavaObjectBis.hxx"
#include "unwrap.hpp"

using namespace ScilabObjects;

extern "C"
{
#include "getScilabJavaVM.h"
}

#define ScilabJavaEnvironmentWrap(Type, type) int ScilabJavaEnvironmentWrapper::wrap##Type(type x) \
	{ \
		JavaVM *vm = getScilabJavaVM(); \
		if(vm) \
			return ScilabObjects::ScilabJavaObject::wrap##Type(vm, x); \
		return -1;	\
	} \
	int ScilabJavaEnvironmentWrapper::wrap##Type(type* x, int xSize) \
	{ \
		JavaVM *vm = getScilabJavaVM(); \
		if(vm) \
			return ScilabObjects::ScilabJavaObject::wrap##Type(vm, x, xSize); \
		return -1;	\
	} \
	int ScilabJavaEnvironmentWrapper::wrap##Type(type** x, int xSize, int xColSize) \
	{ \
		JavaVM *vm = getScilabJavaVM(); \
		if(vm) \
			return ScilabObjects::ScilabJavaObject::wrap##Type(vm, x, xSize, xColSize); \
		return -1;	\
	}

#define ScilabJavaEnvironmentUnwrap(kind,Kind,type,ScilabType,JNIType,Type) void ScilabJavaEnvironmentWrapper::unwrap##kind##type (int id, int pos) \
	{                                                                   \
		JavaVM *vm_ = getScilabJavaVM ();                               \
		if (vm_)                                                        \
		{                                                               \
			try                                                         \
			{                                                           \
				unwrap##Kind<JNIType,ScilabType,__JIMS__Scilab##Type##__>(vm_, getMethodOfConv(), id, pos); \
			}                                                           \
			catch (char const *e)                                       \
			{                                                           \
				throw ScilabAbstractEnvironmentException(e);            \
			}                                                           \
		}                                                               \
	}

#define ScilabJavaEnvironmentWrapUnwrapForType(type,ScilabType,JNIType,Type) ScilabJavaEnvironmentUnwrap(,Single,type,ScilabType,JNIType,Type) \
	ScilabJavaEnvironmentUnwrap(row,Row,type,ScilabType,JNIType,Type)                        \
	ScilabJavaEnvironmentUnwrap(mat,Mat,type,ScilabType,JNIType,Type)

ScilabJavaEnvironmentWrap(Double, double)
ScilabJavaEnvironmentWrap(Int, int)
ScilabJavaEnvironmentWrap(UInt, long long)
ScilabJavaEnvironmentWrap(Short, short)
ScilabJavaEnvironmentWrap(UShort, int)
ScilabJavaEnvironmentWrap(Boolean, bool)
ScilabJavaEnvironmentWrap(Char, unsigned short)
ScilabJavaEnvironmentWrap(Float, float)
ScilabJavaEnvironmentWrap(Long, long long)
ScilabJavaEnvironmentWrap(String, char*)
ScilabJavaEnvironmentWrap(Byte, byte)
ScilabJavaEnvironmentWrap(UByte, short)

ScilabJavaEnvironmentWrapUnwrapForType(double,double,jdouble,Double)
ScilabJavaEnvironmentWrapUnwrapForType(int,int,jint,Int)
ScilabJavaEnvironmentWrapUnwrapForType(boolean,int,jboolean,Boolean)
ScilabJavaEnvironmentWrapUnwrapForType(byte,char,jbyte,Byte)
ScilabJavaEnvironmentWrapUnwrapForType(short,short,jshort,Short)
ScilabJavaEnvironmentWrapUnwrapForType(char,unsigned short,jchar,Char)
ScilabJavaEnvironmentWrapUnwrapForType(long,long,jlong,Long)
ScilabJavaEnvironmentWrapUnwrapForType(float,double,jfloat,Float)

/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironmentWrapper::unwrapstring(int id, int pos)
{
    JavaVM *vm = getScilabJavaVM();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::unwrapString(vm, id, pos);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironmentWrapper::unwraprowstring(int id, int pos)
{
    JavaVM *vm = getScilabJavaVM();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::unwrapRowString(vm, id, pos);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironmentWrapper::unwrapmatstring(int id, int pos)
{
    JavaVM *vm = getScilabJavaVM();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::unwrapMatString(vm, id, pos);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
}