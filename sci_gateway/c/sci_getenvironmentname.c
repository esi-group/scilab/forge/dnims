/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_getenvironmentname(char *fname)
{
    SciErr err;
	int row = 0;
	int col = 0;
	int *envId = 0;
    int *addr = NULL;
    int idObj = 0;
    char *errmsg = NULL;
	char* envName = NULL;

    CheckRhs(1, 1);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

	if (!isEnvObj(addr) && !isEnvClass(addr))
	{
		Scierror(999, "%s: Wrong type for argument 1 : _EObj or _EClass expected\n", fname);
		return 0;
	}

	err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &envId);
	if (err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	envName = getenvironmentname(*envId);
    err = createMatrixOfString(pvApiCtx, Rhs + 1, 1, 1, (const char * const *)&envName);

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
