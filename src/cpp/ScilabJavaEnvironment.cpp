/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be usesd under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/* ------------------------------------------------------------------------- */
#include "ScilabJavaEnvironment.hxx"
/* ------------------------------------------------------------------------- */
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "ScilabObjects.hxx"
#include <string.h>
#include "ScilabClassLoader.hxx"
#include "ScilabJavaClass.hxx"
#include "ScilabJavaObjectBis.hxx"
#include "ScilabJavaArray.hxx"
#include "ScilabJavaCompiler.hxx"
#include "GiwsException.hxx"
#include "NoMoreScilabMemoryException.hxx"
#include "wrap.hpp"
#include "wrapwithcast.hpp"
#include "unwrap.hpp"
#include "WrapAsDirectBufferTemplate.hpp"
#ifdef _MSC_VER
#include "strdup_windows.h"
#endif

using namespace ScilabObjects;
/* ------------------------------------------------------------------------- */
ScilabJavaEnvironment::ScilabJavaEnvironment()
{
	JavaVM *vm = getScilabJavaVM ();
	if (vm)
	{
		try
		{
			ScilabJavaObject::initScilabJavaObject(vm);
		}
		catch (GiwsException::JniException e)
		{
			throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
		}
		catch (ScilabObjects::NoMoreScilabMemoryException e)
		{
			throw ScilabAbstractEnvironmentException(e.what());
		}
	}
}

void ScilabJavaEnvironment::garbagecollect()
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObject::garbageCollect(vm);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::createarray(char *className, int* dims, int len)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaArray::newInstance(vm, className, dims, len);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::loadclass(char *className, char allowReload)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabClassLoader::loadJavaClass(vm, className, getAllowReload() == 0 ? false : true);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());            
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
char* ScilabJavaEnvironment::getrepresentation(int id)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            char *rep = ScilabJavaObject::getRepresentation(vm, id);
            char *returnedRep = NULL;
            if (rep)
            {
                returnedRep = strdup(rep);
            }
            return returnedRep;
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());             
        }
    }

    return NULL;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::isvalidobject(int id)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        bool b = ScilabJavaObject::isValidJavaObject(vm, id);
        return b ? 1 : 0;
    }

    return 0;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::newinstance(int id, int *args, int argsSize)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaClass::newInstance(vm, id, args, argsSize);
        }
        catch (GiwsException::JniException e)
        {
                        throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::invoke(int id, char *methodName, int *args, int argsSize)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::invoke(vm, id, methodName, args, argsSize);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::setfield(int id, char *fieldName, int idarg)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::setField(vm, id, fieldName, idarg);
        }
        catch (GiwsException::JniException e)
        {
                       throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }    
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::getfield(int id, char *fieldName)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::getField(vm, id, fieldName);
        }
        catch (GiwsException::JniException e)
        {
                        throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::getfieldtype(int id, char *fieldName)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::getFieldType(vm, id, fieldName);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::getarrayelement(int id, int *index, int length)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::getArrayElement(vm, id, index, length);
        }
        catch (GiwsException::JniException e)
        {
                        throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::setarrayelement(int id, int *index, int length, int idArg)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObject::setArrayElement(vm, id, index, length, idArg);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::cast(int id, char *objName)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::javaCast(vm, id, objName);
        }
        catch (GiwsException::JniException e)
        {
                       throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::castwithid(int id, int classId)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::javaCast(vm, id, classId);
        }
        catch (GiwsException::JniException e)
        {
                        throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::removescilabobject(int id)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        ScilabJavaObject::removeScilabJavaObject(vm, id);
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::getaccessiblemethods(int id, int pos)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::getMethodResult(vm, "getAccessibleMethods", id, pos);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::getaccessiblefields(int id, int pos)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::getMethodResult(vm, "getAccessibleFields", id, pos);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
}
/* ------------------------------------------------------------------------- */
char* ScilabJavaEnvironment::getclassname(int id)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return strdup(ScilabJavaObject::getClassName(vm, id));
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
        catch (ScilabObjects::NoMoreScilabMemoryException e)
        {
            throw ScilabAbstractEnvironmentException(e.what());
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::isunwrappable(int id)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaObject::isUnwrappable(vm, id);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
int ScilabJavaEnvironment::compilecode(char *className, char **code, int size)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            return ScilabJavaCompiler::compileCode(vm, className, code, size);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }

    return -1;
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::releasedirectbuffer(void **ref, int* id, int len)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObjectBis::releaseDirectBuffer(vm, ref, id, len);
        }
        catch (GiwsException::JniException e)
        {
            	throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::enabletrace(char *filename)
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        try
        {
            ScilabJavaObject::enableTrace(vm, filename);
        }
        catch (GiwsException::JniException e)
        {
            throw ScilabAbstractEnvironmentException(e.getJavaDescription().c_str());
        }
    }
}
/* ------------------------------------------------------------------------- */
void ScilabJavaEnvironment::disabletrace()
{
    JavaVM *vm = getScilabJavaVM ();
    if (vm)
    {
        ScilabJavaObject::disableTrace(vm);
    }
}
/* ------------------------------------------------------------------------- */