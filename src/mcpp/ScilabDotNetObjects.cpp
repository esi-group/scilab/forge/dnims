#define SDNO_LINKAGE __declspec(dllexport)
#include "ScilabDotNetObjects.h"
#include "ScilabDotNetType.h"
#include "ScilabAbstractEnvironmentException.hxx"
#include <iostream>
#include <queue>

#include <Windows.h>

//Type - .NET type, type - native type
#define DNUnwrapForTypeImpl(StType, type, TName) type ScilabDotNetObjects::unwrap##TName(const int& id) \
{ \
    Object* sotoredObj = NULL; \
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &sotoredObj)) \
        THROW_INVALID_OBJ \
 \
        return *(dynamic_cast<__box StType*>(sotoredObj)); \
} \
type* ScilabDotNetObjects::unwrapRow##TName(const int& id, int& len) \
{ \
    Object* sotoredObj = NULL; \
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &sotoredObj)) \
        THROW_INVALID_OBJ \
 \
        StType x_[] = dynamic_cast<StType[]>(sotoredObj); \
 \
    len = x_->Length; \
    type* natArray = new type[len]; \
 \
    for (int i=0; i<len; ++i) \
    { \
        natArray[i] = x_[i]; \
    } \
 \
    return natArray; \
} \
type* ScilabDotNetObjects::unwrapMat##TName(const int& id, int& lenCol, int& lenRow, int convertMatrixMethod) \
{ \
    Object* sotoredObj = NULL; \
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &sotoredObj)) \
        THROW_INVALID_OBJ \
 \
    StType x_[,] = dynamic_cast<StType[,]>(sotoredObj); \
 \
    lenRow = x_->GetLength(0); \
    lenCol = x_->GetLength(1); \
    type* natArray = new type[lenCol * lenRow]; \
 \
    for (int i=0; i<lenRow; ++i) \
    { \
        if(convertMatrixMethod) \
        { \
            for (int j=0; j<lenCol; ++j) \
            { \
                natArray[j*lenRow + i] = x_[i, j]; \
            } \
        } \
        else \
        { \
            for (int j=0; j<lenCol; ++j) \
            { \
                natArray[i*lenCol + j] = x_[i, j]; \
            } \
        } \
    }     \
 \
    return natArray; \
}

#using <mscorlib.dll>
#using <System.dll>

#define RETHROW_SYSTEM_EXCEPTION catch(Exception *ex) \
    { \
        throw ScilabAbstractEnvironmentException((char*)(void*)Marshal::StringToHGlobalAnsi(ex->Message)); \
    }

#define THROW_INVALID_OBJ throw std::exception("Provided id is not id of a valid object");

template <typename T1, typename T2>
inline bool istypeof ( T2* t )
{
    return (dynamic_cast<T1*>(t) != 0);
}

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace System::Diagnostics;

__gc class ScilabDotNetObjectsStorage{
public:    
    static const long long INITIAL_IDS_VOLUME = 1024;
    static Dictionary<String*, Assembly*>* assemblies = NULL;
    static Dictionary<int, Object*>* objects = NULL;
    static Dictionary<String*, int>* types = NULL;
    static int maxId = INITIAL_IDS_VOLUME;
    static std::queue<int>* freeIds = NULL;
    static Dictionary<Type*, ScilabDotNetType*>* cachedTypes = NULL;
    static List<Type*>* basicTypes = NULL;
    static Dictionary<String*, String*>* longNames = NULL;

    static Assembly* AssemblyResolveHandler(Object* source, ResolveEventArgs* e) 
    {
        throw ScilabAbstractEnvironmentException((char*)(void*)Marshal::StringToHGlobalAnsi(String::Concat("Dependencies of assembly ", e->Name, " could not be resolved. Check .NET framework version correspondence and availability of referenced assemblies.")));
    }

    static void Initialize()
    {
        objects = new Dictionary<int, Object*>();
        assemblies = new Dictionary<String*, Assembly*>();
        types = new Dictionary<String*, int>();
        freeIds = new std::queue<int>();
        cachedTypes = new Dictionary<Type*, ScilabDotNetType*>();

        basicTypes = new List<Type*>();
        basicTypes->Add(__typeof(Double));
        basicTypes->Add(__typeof(String));
        basicTypes->Add(__typeof(Int32));
        basicTypes->Add(__typeof(Boolean));
        basicTypes->Add(__typeof(Char));
        basicTypes->Add(__typeof(Char));
        basicTypes->Add(__typeof(float));
#ifdef __SCILAB_INT64__
        basicTypes->Add(__typeof(Int64));
#else
        basicTypes->Add(__typeof(UInt32));
#endif

        //add null object, to be referred from Scilab as dnnull
        objects->Add(0, NULL);

        initFreeIds();

        AppDomain* curDomain = AppDomain::CurrentDomain;
        curDomain->AssemblyResolve += new ResolveEventHandler(AssemblyResolveHandler);

        longNames = new Dictionary<String*, String*>();
        longNames->Add("System.configuration", "System.configuration, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Configuration.Install", "System.Configuration.Install, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Data", "System.Data, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Data.OracleClient", "System.Data.OracleClient, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Data.SqlXml", "System.Data.SqlXml, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Deployment", "System.Deployment, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Design", "System.Design, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.DirectoryServices", "System.DirectoryServices, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.DirectoryServices.Protocols", "System.DirectoryServices.Protocols, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System", "System, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Drawing.Design", "System.Drawing.Design, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Drawing", "System.Drawing, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.EnterpriseServices", "System.EnterpriseServices, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Management", "System.Management, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Messaging", "System.Messaging, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Runtime.Remoting", "System.Runtime.Remoting, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Runtime.Serialization.Formatters.Soap", "System.Runtime.Serialization.Formatters.Soap, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Security", "System.Security, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.ServiceProcess", "System.ServiceProcess, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Transactions", "System.Transactions, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.Web", "System.Web, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Web.Mobile", "System.Web.Mobile, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Web.RegularExpressions", "System.Web.RegularExpressions, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Web.Services", "System.Web.Services, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
        longNames->Add("System.Windows.Forms", "System.Windows.Forms, Version=2.0.50727.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
        longNames->Add("System.XML", "System.XML, Version=2.0.50727.0, Culture=neutral, PublicKeytoken=b77a5c561934e089");

    }

    static int AddObject(Object* newObj)
    {
        int nId = getFreeId();
        objects->Add(nId, newObj);

        if (newObj->GetType()->Equals(__typeof(Type*)))
        {
            if(!cachedTypes->ContainsKey(dynamic_cast<Type*>(newObj)))
                cachedTypes->Add(newObj, new ScilabDotNetType(dynamic_cast<Type*>(newObj)));
        }

        return nId;
    }

    static bool RemoveObject(int id)
    {
        if(objects->ContainsKey(id))
        {    
            objects->Remove(id);
            freeIds->push(id);
            return true;
        }
        else
            return false;
    }

    static void FreeAll()
    {
        objects->Clear();
        types->Clear();
        initFreeIds();
    }

private:
    static int getFreeId()
    {
        if(!freeIds->empty())
        {
            int id = freeIds->front();
            freeIds->pop();
            return id;
        }
        else
        {
            extendFreeIds();
            return getFreeId();
        }
    }

    static void extendFreeIds()
    {
        for (int i = maxId + 1; i <= 2 * maxId; ++i)
        {
            freeIds->push(i);
        }
        maxId *= 2;
    }

    static void initFreeIds()
    {
        if(!freeIds)
            freeIds = new std::queue<int>();

        while(!freeIds->empty())
            freeIds->pop();

        maxId = INITIAL_IDS_VOLUME;
        for (int i=1; i<=maxId; ++i)
        {
            freeIds->push(i);
        }
    }
};

Type* GetTypeByName(String* typeName)
{
    Type* sysType = Type::GetType(typeName);

    if(!sysType)
    {
        //looking for type in all loaded assemblies
        Dictionary<String*, Assembly*>::Enumerator enumerator = ScilabDotNetObjectsStorage::assemblies->GetEnumerator();
        while(enumerator.MoveNext())
        {
            KeyValuePair<String*, Assembly*> kpAsm = enumerator.Current;
            if (sysType = kpAsm.Value->GetType(typeName))
            {
                break;
            }
        }
    }

    return sysType;
}

void ScilabDotNetObjects::Initialize()
{
    try
    {
        ScilabDotNetObjectsStorage::Initialize();
    }
    RETHROW_SYSTEM_EXCEPTION
}

void loadOrThrow(String* sQualifiedName)
{
    try
    {
        Assembly* loadedAsm = Assembly::Load(sQualifiedName);
        if(loadedAsm)
            ScilabDotNetObjectsStorage::assemblies->Add(sQualifiedName, loadedAsm);
        else
            throw ScilabAbstractEnvironmentException((char*)(void*)Marshal::StringToHGlobalAnsi(String::Concat("Assembly with a given qualified name ", sQualifiedName, " was not found")));
    }
    RETHROW_SYSTEM_EXCEPTION
}

void loadFromOrThrow(String* sAsmPath)
{
    try
    {
        Assembly* loadedAsm = Assembly::LoadFrom(sAsmPath);
        if(loadedAsm)
            ScilabDotNetObjectsStorage::assemblies->Add(sAsmPath, loadedAsm);
        else
            throw ScilabAbstractEnvironmentException((char*)(void*)Marshal::StringToHGlobalAnsi(String::Concat("Assembly not found under path ", sAsmPath)));
    }
    RETHROW_SYSTEM_EXCEPTION
}

void ScilabDotNetObjects::Load(const char* qualifiedName, bool allowReload)
{
    try
    {
        String *sQualifiedName =  Marshal::PtrToStringAnsi(const_cast<char*>(qualifiedName));

        if(!sQualifiedName->Contains("PublicKeyToken"))
        {
            if (ScilabDotNetObjectsStorage::longNames->ContainsKey(sQualifiedName))
            {
                String* sQualifiedNameShort = sQualifiedName;
                ScilabDotNetObjectsStorage::longNames->TryGetValue(sQualifiedNameShort, &sQualifiedName);
            }
            else
            {
                throw ScilabAbstractEnvironmentException("You must either provide long name of assembly includeing public key token, or use loadfrom");
            }
        }

        if(!ScilabDotNetObjectsStorage::assemblies->ContainsKey(sQualifiedName))
        {
            loadOrThrow(sQualifiedName);
        }
        else if (allowReload)
        {
            ScilabDotNetObjectsStorage::assemblies->Remove(sQualifiedName);
            loadOrThrow(sQualifiedName);
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

void ScilabDotNetObjects::LoadFrom(const char* dllPath, bool allowReload)
{
    try
    {
        String *sAsmPath =  Marshal::PtrToStringAnsi(const_cast<char*>(dllPath));
        if(!ScilabDotNetObjectsStorage::assemblies->ContainsKey(sAsmPath))
        {
            loadFromOrThrow(sAsmPath);
        }
        else if(allowReload)
        {
            ScilabDotNetObjectsStorage::assemblies->Remove(sAsmPath);
            loadFromOrThrow(sAsmPath);
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::LoadClass(char* typeName, bool allowReload)
{
    try
    {
        String *sTypeName =  Marshal::PtrToStringAnsi(typeName);        

        Type* typeObj = NULL;
        int idTypeObj;
        if(ScilabDotNetObjectsStorage::types->TryGetValue(sTypeName, &idTypeObj) && !allowReload)
        {
            return idTypeObj;
        }

        typeObj = GetTypeByName(sTypeName);

        if(typeObj)
        {
            //removing from collections, because class is either being reloaded or was not found
            if(ScilabDotNetObjectsStorage::cachedTypes->ContainsKey(typeObj))
                ScilabDotNetObjectsStorage::cachedTypes->Remove(typeObj);

            if(ScilabDotNetObjectsStorage::types->ContainsKey(sTypeName))
                ScilabDotNetObjectsStorage::types->Remove(sTypeName);

            //cannot remove type from objects collection, because other objects can still reference it
            //this gives just minor overhead, but it is more secure

            int iNewObj = ScilabDotNetObjectsStorage::AddObject(typeObj);
            ScilabDotNetObjectsStorage::types->Add(sTypeName, iNewObj);
            ScilabDotNetObjectsStorage::cachedTypes->Add(typeObj, new ScilabDotNetType(typeObj));
            return iNewObj;
        }
        else
        {
            return -1;
        }

        return -1;
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::NewInstance(int classId, int* argv, int argc)
{
    try
    {
        Object* typeObj;
        Type* newObjType;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(classId, &typeObj))
        {
            throw std::exception("Provided id is not valid id of an object");
        }
        newObjType = dynamic_cast<Type*>(typeObj);
        if(!newObjType)
        {
            throw std::exception("Provided id is not valid id of a class");
        }

        Type* argTypes[] = argc>0 ? new Type*[argc] : new Type*[]{};
        Object* constrArgs[] = argc>0 ? new Object*[argc] : new Object*[]{};
        for (int i=0; i<argc; ++i)
        {
            if(!ScilabDotNetObjectsStorage::objects->TryGetValue(argv[i], &constrArgs[i]))
                throw std::exception("Provided id is not valid id of an object");

            argTypes[i] = constrArgs[i]->GetType();
        }
        
        Object* newObj = NULL;
        if(newObjType->IsValueType)
        {
            newObj = Activator::CreateInstance(newObjType, constrArgs);
        }
        else
        {
            ConstructorInfo *constrInf = newObjType->GetConstructor(argTypes);            
            if (!constrInf)
            {                
                throw std::exception("No suitable constructor for arguments of given types");
            }
            newObj = constrInf->Invoke(constrArgs);
        }
        
        return newObj ? ScilabDotNetObjectsStorage::AddObject(newObj) : 0;
    }
    RETHROW_SYSTEM_EXCEPTION
}

bool ScilabDotNetObjects::IsValidDotNetObject(int id)
{
    try
    {
        return ScilabDotNetObjectsStorage::objects->ContainsKey(id);
    }
    RETHROW_SYSTEM_EXCEPTION
}

bool ScilabDotNetObjects::RemoveDotNetObject(int id)
{
    try
    {
        if (!IsValidDotNetObject(id))
        {
            throw std::exception("Provided id is not valid id of an object");
        }

        return ScilabDotNetObjectsStorage::RemoveObject(id);
    }
    RETHROW_SYSTEM_EXCEPTION
}

char* ScilabDotNetObjects::GetObjectRepresentation(int id)
{
    try
    {
        Object* storedObj;    
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &storedObj))
        {
            throw std::exception("Provided id is not valid id of an object");
        }

        if(storedObj)
        {
            if (istypeof<Type>(storedObj))
            {
                return (char*)(void*)Marshal::StringToHGlobalAnsi(String::Concat(".NET class ", storedObj->ToString()));
            }
            else
            {
                return (char*)(void*)Marshal::StringToHGlobalAnsi(storedObj->ToString());
            }
        }
        else
        {
            return (char*)(void*)Marshal::StringToHGlobalAnsi("null");
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

void ScilabDotNetObjects::GarbageCollect()
{
    try
    {
        ScilabDotNetObjectsStorage::FreeAll();
        //collect garbage of all generations
        GC::Collect();
    }    
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::CreateArray(char* className, int* dims, int len)
{
    try
    {
        if(len < 1 || len > 32)
        {
            throw std::exception("Array dimension must be between 1 and 32");
        }

        String *sTypeName =  Marshal::PtrToStringAnsi(className);
        Type* typeObj = GetTypeByName(sTypeName);    

        Int32 idims[] = new Int32[len];
        for (int i=0; i<len;++i)
        {
            idims[i] = dims[i];
        }
        Object* arrObj = Array::CreateInstance(typeObj, idims);

        return ScilabDotNetObjectsStorage::AddObject(arrObj);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::Invoke(int id, char* methodName, int* argv, int argc)
{
    try
    {
        String *sMethodName =  Marshal::PtrToStringAnsi(methodName);

        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ

        Type* argTypes[] = new Type*[argc];
        Object* args[] = new Object*[argc];
        for (int i=0;i<argc;++i)
        {
            Object* argObj = NULL;
            if(!ScilabDotNetObjectsStorage::objects->TryGetValue(argv[i], &argObj))
                THROW_INVALID_OBJ

            argTypes[i] = argObj->GetType();
            args[i] = argObj;
        }

        bool isType = false;
        Type* targObjectType = dynamic_cast<Type*>(targetObject);
        if(!targObjectType)
        {
            targObjectType = targetObject->GetType();
        }
        else
        {
            isType = true;
        }

        MethodInfo* mInfo = targObjectType->GetMethod(sMethodName, argTypes);
        if(!mInfo)
            throw ScilabAbstractEnvironmentException("Method for arguments of provided types cannot be found");

        Object* result = mInfo->Invoke(isType ? NULL : targetObject, args);
        return ScilabDotNetObjectsStorage::AddObject(result);
    }
    RETHROW_SYSTEM_EXCEPTION
}

void ScilabDotNetObjects::SetField(int id, char *fieldName, int idArg)
{
    try
    {
        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ

        Object* fieldObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(idArg, &fieldObject))
            THROW_INVALID_OBJ

        String *sFieldName =  Marshal::PtrToStringAnsi(fieldName);

        bool isType = false;
        Type* objType = dynamic_cast<Type*>(targetObject);
        if(objType)
        {
            isType = true;
        }
        else
        {
            objType = targetObject->GetType();
        }

        FieldInfo* fInfo = objType->GetField(sFieldName);
        if(fInfo)
        {
            fInfo->SetValue(isType ? NULL : targetObject, fieldObject);
        }
        else
        {
            PropertyInfo* pInfo = objType->GetProperty(sFieldName);
            pInfo->SetValue(isType ? NULL : targetObject, fieldObject, NULL);
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::GetField(int id, char *fieldName)
{
    try
    {
        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ    

        String *sFieldName =  Marshal::PtrToStringAnsi(fieldName);

        bool isType = false;
        Type* objType = dynamic_cast<Type*>(targetObject);
        if(objType)
        {
            isType = true;
        }
        else
        {
            objType = targetObject->GetType();
        }

        FieldInfo* fInfo = objType->GetField(sFieldName);
        if(!fInfo)
        {
            PropertyInfo* pInfo = objType->GetProperty(sFieldName);
            return ScilabDotNetObjectsStorage::AddObject(pInfo->GetValue(isType ? NULL : targetObject, NULL));
        }
        else
        {
            return ScilabDotNetObjectsStorage::AddObject(fInfo->GetValue(isType ? NULL : targetObject));
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::GetFieldType(int id, char* fieldName)
{
    try
    {
        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ

        String *sFieldName =  Marshal::PtrToStringAnsi(fieldName);

        Type* storedObjType = dynamic_cast<Type*>(targetObject);
        if(!storedObjType)
        {
            storedObjType = targetObject->GetType();
        }

        ScilabDotNetType* dnType = NULL;
        if(!ScilabDotNetObjectsStorage::cachedTypes->ContainsKey(storedObjType))
            ScilabDotNetObjectsStorage::cachedTypes->Add(storedObjType, new ScilabDotNetType(storedObjType));

        ScilabDotNetObjectsStorage::cachedTypes->TryGetValue(storedObjType, &dnType);

        if (dnType->HasMethod(fieldName))        
            return 0;        
        else if (dnType->HasField(fieldName))
            return 1;
        else
            return -1;
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::GetArrayElement(int id, int* index, int length)
{
    try
    {
        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ

        Array *arrObj = dynamic_cast<Array *>(targetObject);
        if(!arrObj)
            throw std::exception("Provided id is not id of an array object");

        if(arrObj->Rank != length)
            throw std::exception("Bad array rank");

        Int64 indices[] = new Int64[length];
        for (int i=0;i<length;++i, ++index)
        {
            indices[i] = Int64(*index);
        }

        Object* arrElement = arrObj->GetValue(indices);

        return ScilabDotNetObjectsStorage::AddObject(arrElement);
    }
    RETHROW_SYSTEM_EXCEPTION
}

void ScilabDotNetObjects::SetArrayElement(int id, int* index, int length, int idArg)
{
    try
    {
        Object* targetObject = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &targetObject))
            THROW_INVALID_OBJ

        Array *arrObj = dynamic_cast<Array *>(targetObject);
        if(!arrObj)
            throw std::exception("Provided id is not id of an array object");

        Object* argObj = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(idArg, &argObj))
            THROW_INVALID_OBJ

        Int64 indices[] = new Int64[length];
        for (int i=0;i<length;++i, ++index)
        {
            indices[i] = Int64(*index);
        }

        arrObj->SetValue(argObj, indices);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::Cast(int id, char* className)
{
    try
    {
        Type* typeObj = NULL;
        Object* ObjTypeObj = NULL;
        int typeObjId = LoadClass(className, false);

        if(!typeObjId)
            return 0;

        ScilabDotNetObjectsStorage::objects->TryGetValue(typeObjId, &ObjTypeObj);
        typeObj = dynamic_cast<Type*>(ObjTypeObj);

        if (typeObj)
        {
            Object* objToCast = NULL;
            if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &objToCast))
                THROW_INVALID_OBJ

            Object* castedObj = Convert::ChangeType(objToCast, typeObj);
            return ScilabDotNetObjectsStorage::AddObject(castedObj);
        }
        return 0;
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::CastWithId(int id, int typeId)
{
    try
    {
        Object* typeStorageObj = NULL, *objToCast = NULL;
        Type* typeObj;

        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &objToCast))
            THROW_INVALID_OBJ

        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(typeId, &typeStorageObj))
            THROW_INVALID_OBJ

        typeObj = dynamic_cast<Type*>(typeStorageObj);
        if(!typeObj)
            throw std::exception("Type with given id not found");

        Object* castedObj = Convert::ChangeType(objToCast, typeObj);
        return ScilabDotNetObjectsStorage::AddObject(castedObj);
    }
    RETHROW_SYSTEM_EXCEPTION
}

ScilabDotNetType* GetScilabDotNetTypeByObjOrTypeId(int id, bool& is_type)
{
    Type* typeObj = NULL;
    Object* objObj = NULL;

    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &objObj))
        THROW_INVALID_OBJ

    typeObj = dynamic_cast<Type*>(objObj);
    if(!typeObj)
    {
        typeObj = objObj->GetType();
        is_type = false;
    }
    else
    {
        is_type = true;
    }    
    ScilabDotNetType* dnType = NULL;
    if(!ScilabDotNetObjectsStorage::cachedTypes->ContainsKey(typeObj))
    {        
        dnType = new ScilabDotNetType(typeObj);
        ScilabDotNetObjectsStorage::cachedTypes->Add(typeObj, dnType);
    }
    else
    {
        ScilabDotNetObjectsStorage::cachedTypes->TryGetValue(typeObj, &dnType);
    }

    return dnType;
}

char** StringListToCharArray(List<String*>* sList)
{
    char** result = new char*[sList->Count];
    List<String*>::Enumerator lEnum = sList->GetEnumerator();    
    for(int i=0; lEnum.MoveNext(); ++i)    
        result[i] = (char*)(void*)Marshal::StringToHGlobalAnsi(lEnum.Current);

    return result;
}

char** ScilabDotNetObjects::GetAccessibleMethods(int id, int& length)
{
    bool is_type;
    ScilabDotNetType* dnType = GetScilabDotNetTypeByObjOrTypeId(id, is_type);
    List<String*>* methods = is_type ? dnType->GetAccessibleStaticMethodNames() : dnType->GetAccessibleMethodNames();
    length = methods->Count;
    return StringListToCharArray(methods);
}

char** ScilabDotNetObjects::GetAccessibleFields(int id, int& length)
{
    bool is_type;
    ScilabDotNetType* dnType = GetScilabDotNetTypeByObjOrTypeId(id, is_type);
    List<String*>* fields = is_type ? dnType->GetAccessibleStaticFieldNames() : dnType->GetAccessibleFieldNames();
    length = fields->Count;
    return StringListToCharArray(fields);
}

char* ScilabDotNetObjects::GetDotNetClassName(int id)
{
    try
    {    
        Object* storedObj = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &storedObj))
            THROW_INVALID_OBJ

        Type* typeObj = storedObj->GetType();

        String* className = typeObj->Name;

        return (char*)(void*)Marshal::StringToHGlobalAnsi(className->ToString());
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapString(char* str)
{
    try
    {
        String* managedString = Marshal::PtrToStringAnsi(str);
        return ScilabDotNetObjectsStorage::AddObject(managedString);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapString(char** strings, int len)
{
    try
    {
        String* managedStrings[] = new String*[len];
        for (int i=0;i<len;++i, ++strings)
        {
            managedStrings[i] = Marshal::PtrToStringAnsi(*strings);
        }
        return ScilabDotNetObjectsStorage::AddObject(managedStrings);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapString(char*** strings, int rows, int cols)
{
    try
    {
        String* managedStrings __gc[,] = new String* __gc[rows, cols];
        for (int i=0;i<rows;++i, ++strings)
        {
            char** rowStrings = *strings;
            for (int j=0; j<cols;++j, ++rowStrings)
            {
                managedStrings[i, j] = Marshal::PtrToStringAnsi(*rowStrings);
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(managedStrings);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::IsUnwrappable(int id)
{
    try
    {
        Object* storedObj = NULL;
        if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &storedObj))
            THROW_INVALID_OBJ

        Type* storedObjType = storedObj->GetType();
        int arrRank = 0;
        if(storedObjType->IsArray)
        {
            arrRank = storedObjType->GetArrayRank();
            storedObjType = storedObjType->GetElementType();
        }

        if(ScilabDotNetObjectsStorage::basicTypes->Contains(storedObjType) && arrRank < 3)
        {
            return ScilabDotNetObjectsStorage::basicTypes->IndexOf(storedObjType) * 3 + arrRank;
        }
        else
        {
            return -1;
        }
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::CompileCode(char* className, char** code, int size)
{
    try
    {
        //write code to temporary file
        String* tmpFileName = IO::Path::GetTempFileName();
        IO::StreamWriter* writer = new IO::StreamWriter(tmpFileName);

        for (int i=0; i<size; ++i, ++code)
        {
            writer->WriteLine(Marshal::PtrToStringAnsi(*code));
        }
        writer->Close();

        //compile
        String* runtimeDir = Runtime::InteropServices::RuntimeEnvironment::GetRuntimeDirectory();
        ProcessStartInfo* psi = new ProcessStartInfo("csc.exe", String::Concat("/target:library ", tmpFileName));
        psi->UseShellExecute = false;
        Process* cscProc = new Process();

        char* pathVar;
        size_t pathVarLen;
        errno_t err = _dupenv_s(&pathVar, &pathVarLen, "PATH");
        if(err)
            throw ScilabAbstractEnvironmentException("Failed to retrieve PATH environment variable");

        free(pathVar);
        String* pathEnv = Marshal::PtrToStringAnsi(pathVar);

        if(!pathEnv->Contains(runtimeDir))
        {
            pathEnv = String::Concat(pathEnv, ";", runtimeDir);
            Environment::SetEnvironmentVariable("PATH", pathEnv, EnvironmentVariableTarget::Process);
        }

        cscProc->StartInfo = psi;
        cscProc->Start();
 
        //TODO: make timeout settable with macro dnsetcmopiletimeout()
        const int WAIT_MAX = 5000;
         cscProc->WaitForExit(WAIT_MAX);
        IO::File::Delete(tmpFileName);

        //load dll
        String* dllPath = String::Concat(IO::Path::GetFileNameWithoutExtension(tmpFileName), ".dll");
        LoadFrom((char*)(void*)Marshal::StringToHGlobalAnsi(dllPath), true);

        //load compiled class and return it's id
        return LoadClass(className, true);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapDouble(double x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapDouble(double* x, const  int length)
{
    try
    {
        Double x_[] = new Double[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapDouble(double** x, const int xSize, const int xSizeCols)
{
    try
    {
        Double x_[,] = new Double[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            double* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapInt(int x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapInt(int* x, const  int length)
{
    try
    {
        Int32 x_[] = new Int32[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapInt(int** x, const int xSize, const int xSizeCols)
{
    try
    {
        Int32 x_[,] = new Int32[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            int* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapUInt(long long x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapUInt(long long* x, const  int length)
{
    try
    {
        UInt32 x_[] = new UInt32[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapUInt(long long** x, const int xSize, const int xSizeCols)
{
    try
    {
        UInt32 x_[,] = new UInt32[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            long long* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapShort(short x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapShort(short* x, const  int length)
{
    try
    {
        Int16 x_[] = new Int16[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapShort(short** x, const int xSize, const int xSizeCols)
{
    try
    {
        Int16 x_[,] = new Int16[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            short* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapUShort(int x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapUShort(int* x, const  int length)
{
    try
    {
        UInt16 x_[] = new UInt16[length];
        //Marshal::Copy does not have overloads for unsigned arrays
        for (int i=0;i<length;++i,++x)
        {
            x_[i] = *x;
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapUShort(int** x, const int xSize, const int xSizeCols)
{
    try
    {
        UInt16 x_[,] = new UInt16[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            int* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapBoolean(bool x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapBoolean(bool* x, const  int length)
{
    try
    {
        Boolean x_[] = new Boolean[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapBoolean(bool** x, const int xSize, const int xSizeCols)
{
    try
    {
        Boolean x_[,] = new Boolean[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            bool* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapChar(unsigned short x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapChar(unsigned short* x, const  int length)
{
    try
    {
        UInt16 x_[] = new UInt16[length];
        //Marshal::Copy does not have overloads for unsigned arrays
        for (int i=0;i<length;++i,++x)
        {
            x_[i] = *x;
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapChar(unsigned short** x, const int xSize, const int xSizeCols)
{
    try
    {
        UInt16 x_[,] = new UInt16[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            unsigned short* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapFloat(float x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapFloat(float* x, const  int length)
{
    try
    {
        Double x_[] = new Double[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapFloat(float** x, const int xSize, const int xSizeCols)
{
    try
    {
        Double x_[,] = new Double[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            float* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapLong(long long x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapLong(long long* x, const  int length)
{
    try
    {
        Int64 x_[] = new Int64[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapLong(long long** x, const int xSize, const int xSizeCols)
{
    try
    {
        Int64 x_[,] = new Int64[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            long long* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapByte(byte x)
{
    try
    {
        return ScilabDotNetObjectsStorage::AddObject(__box(x));
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapByte(byte* x, const  int length)
{
    try
    {
        Byte x_[] = new Byte[length];
        Marshal::Copy(IntPtr((void*)x), x_, 0, length);
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}
int ScilabDotNetObjects::wrapByte(byte** x, const int xSize, const int xSizeCols)
{
    try
    {
        Byte x_[,] = new Byte[xSize, xSizeCols];
        for (int i=0; i<xSize; ++i, ++x)
        {
            byte* xRow = *x;
            for (int j=0; j<xSizeCols;++j, ++xRow)
            {
                x_[i,j] = *xRow;
            }
        }
        return ScilabDotNetObjectsStorage::AddObject(x_);
    }
    RETHROW_SYSTEM_EXCEPTION
}

int ScilabDotNetObjects::wrapUByte(short x)
{
    return wrapShort(x);
}
int ScilabDotNetObjects::wrapUByte(short* x, const  int length)
{
    return wrapShort(x, length);
}
int ScilabDotNetObjects::wrapUByte(short** x, const int xSize, const int xSizeCols)
{
    return wrapShort(x, xSize, xSizeCols);
}

char* ScilabDotNetObjects::unwrapString(const int& id)
{
    Object* storedObj = NULL;
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &storedObj))
        THROW_INVALID_OBJ

    String* strObj = dynamic_cast<String*>(storedObj);
    return (char*)(void*)Marshal::StringToHGlobalAnsi(strObj);
}

char** ScilabDotNetObjects::unwrapRowString(const int& id, int& len)
{
    Object* storedObj = NULL;
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &storedObj))
        THROW_INVALID_OBJ

    String* strObj[] = dynamic_cast<String*[]>(storedObj);

    len = strObj->Length;
    char** strArray = new char*[len];

    for (int i=0; i<len; ++i)
    {
        strArray[i] = (char*)(void*)Marshal::StringToHGlobalAnsi(strObj[i]);
    }

    return strArray;
}

char** ScilabDotNetObjects::unwrapMatString(const int& id, int& lenCol, int& lenRow, int convertMatrixMethod)
{
    Object* sotoredObj = NULL;
    if(!ScilabDotNetObjectsStorage::objects->TryGetValue(id, &sotoredObj))
        THROW_INVALID_OBJ

    String* x_[,] = dynamic_cast<String*[,]>(sotoredObj);

    lenRow = x_->GetLength(0);
    lenCol = x_->GetLength(1);
    char** natArray = new char*[lenCol * lenRow];

    for (int i=0; i<lenRow; ++i)
    {
        if(convertMatrixMethod)
        {
            for (int j=0; j<lenCol; ++j)
            {
                natArray[j*lenRow + i] = (char*)(void*)Marshal::StringToHGlobalAnsi(x_[i, j]);
            }
        }
        else
        {
            for (int j=0; j<lenCol; ++j)
            {
                natArray[i*lenCol + j] = (char*)(void*)Marshal::StringToHGlobalAnsi(x_[i,j]);
            }
        }
    }

    return natArray;
}

DNUnwrapForTypeImpl(Double, double, Double)
DNUnwrapForTypeImpl(Int32, int, Int)
DNUnwrapForTypeImpl(Int16, short, Short)
DNUnwrapForTypeImpl(Boolean, int, Boolean)
DNUnwrapForTypeImpl(Char, unsigned short, Char)
#pragma  warning(push)
//disable "possible data loss" warning: conversion is deliberate
#pragma warning( disable : 4244 )
DNUnwrapForTypeImpl(Double, float, Float)
DNUnwrapForTypeImpl(Char, char, Byte)
#pragma  warning(pop)
#ifdef __SCILAB_INT64__
DNUnwrapForTypeImpl(Int64, long, Long)
#else
DNUnwrapForTypeImpl(UInt32, unsigned int, Long)
#endif
