//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

jimport java.lang.Double;

jimport java.lang.Number;
if ~isdef("Number") then pause, end

d = Double.new("1.23456");
if typeof(d) <> "_EObj" then pause, end
if jgetclassname(d) <> "java.lang.Double" then pause, end

e = jcast(d, "java.lang.Number");
if typeof(e) <> "_EObj" then pause, end

f = jcast(d, Number);
if typeof(f) <> "_EObj" then pause, end

if f.equals(e) <> jwrap(%t) then pause, end

if jgetclassname(e) <> "java.lang.Number" then pause, end

jremove e d f;
