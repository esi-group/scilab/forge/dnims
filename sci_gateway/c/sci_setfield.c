/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_setfield(char *fname)
{
    SciErr err;
    int tmpvar[3] = {0, 0, 0};
    int *addr = NULL;
    int idObj = 0;
    char *errmsg = NULL;
    char *fieldName = NULL;
    int arg = 0;

    CheckRhs(3, 3);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

    fieldName = getSingleString(2, fname);
    if (!fieldName)
    {
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 3, &addr);
    if (err.iErr)
    {
        freeAllocatedSingleString(fieldName);
        printError(&err, 0);
        return 0;
    }

    arg = getIdOfArg(addr, fname, tmpvar, 0, 3);
    if (arg == - 1)
    {
        freeAllocatedSingleString(fieldName);
        return 0;
    }

    setfield(fname, idObj, fieldName, arg, &errmsg);
    freeAllocatedSingleString(fieldName);

    removeTemporaryVars(fname, tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    LhsVar(1) = 0;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
