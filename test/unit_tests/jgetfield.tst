//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

c = jcompile("Test", ["public class Test {";
                      "public int field;";
                      "public Test(int n) {";
                      "field = n;";
                      "}";
                      "}";]);
t = c.new(128);
v = jgetfield(t, "field");

if typeof(v) <> "_EObj" then pause, end
if jgetclassname(v) <> "int" then pause, end
if junwrap(v) <> 128 then pause, end
