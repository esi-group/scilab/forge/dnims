#ifndef __SCILABABSTRACTENVIRONEMNTWRAPPER_H__
#define __SCILABABSTRACTENVIRONEMNTWRAPPER_H__

#ifndef _MSC_VER /* Defined anyway with Visual */
#if !defined(byte)
typedef signed char byte;
#else
#pragma message("Byte has been redefined elsewhere. Some problems can happen")
#endif
#else
typedef signed char byte;
#endif

class ScilabAbstractEnvironmentWrapper
{
public:
	virtual int wrapDouble(double x) = 0;

	virtual int wrapDouble(double* x, int xSize) = 0;

	virtual int wrapDouble(double** x, int xSize, int xSizeCol) = 0;

	virtual int wrapInt(int x) = 0;

	virtual int wrapInt(int* x, int xSize) = 0;

	virtual int wrapInt(int** x, int xSize, int xSizeCol) = 0;

	virtual int wrapUInt(long long x) = 0;

	virtual int wrapUInt(long long* x, int xSize) = 0;

	virtual int wrapUInt(long long** x, int xSize, int xSizeCol) = 0;

	virtual int wrapByte(byte x) = 0;

	virtual int wrapByte(byte* x, int xSize) = 0;

	virtual int wrapByte(byte** x, int xSize, int xSizeCol) = 0;

	virtual int wrapUByte(short x) = 0;

	virtual int wrapUByte(short* x, int xSize) = 0;

	virtual int wrapUByte(short** x, int xSize, int xSizeCol) = 0;

	virtual int wrapShort(short x) = 0;

	virtual int wrapShort(short* x, int xSize) = 0;

	virtual int wrapShort(short** x, int xSize, int xSizeCol) = 0;

	virtual int wrapUShort(int x) = 0;

	virtual int wrapUShort(int* x, int xSize) = 0;

	virtual int wrapUShort(int** x, int xSize, int xSizeCol) = 0;

	virtual int wrapString(char * x) = 0;

	virtual int wrapString(char ** x, int xSize) = 0;

	virtual int wrapString(char *** x, int xSize, int xSizeCol) = 0;

	virtual int wrapBoolean(bool x) = 0;

	virtual int wrapBoolean(bool* x, int xSize) = 0;

	virtual int wrapBoolean(bool** x, int xSize, int xSizeCol) = 0;

	virtual int wrapChar(unsigned short x) = 0;

	virtual int wrapChar(unsigned short* x, int xSize) = 0;

	virtual int wrapChar(unsigned short** x, int xSize, int xSizeCol) = 0;

	virtual int wrapFloat(float x) = 0;

	virtual int wrapFloat(float* x, int xSize) = 0;

	virtual int wrapFloat(float** x, int xSize, int xSizeCol) = 0;

	virtual int wrapLong(long long x) = 0;

	virtual int wrapLong(long long* x, int xSize) = 0;

	virtual int wrapLong(long long** x, int xSize, int xSizeCol) = 0;

	virtual void unwrapdouble(int, int) = 0;
	virtual void unwraprowdouble(int, int) = 0;
	virtual void unwrapmatdouble(int, int) = 0;
	virtual void unwrapbyte(int, int) = 0;
	virtual void unwraprowbyte(int, int) = 0;
	virtual void unwrapmatbyte(int, int) = 0;
	virtual void unwrapshort(int, int) = 0;
	virtual void unwraprowshort(int, int) = 0;
	virtual void unwrapmatshort(int, int) = 0;
	virtual void unwrapint(int, int) = 0;
	virtual void unwraprowint(int, int) = 0;
	virtual void unwrapmatint(int, int) = 0;
	virtual void unwrapboolean(int, int) = 0;
	virtual void unwraprowboolean(int, int) = 0;
	virtual void unwrapmatboolean(int, int) = 0;
	virtual void unwrapstring(int, int) = 0;
	virtual void unwraprowstring(int, int) = 0;
	virtual void unwrapmatstring(int, int) = 0;
	virtual void unwrapchar(int, int) = 0;
	virtual void unwraprowchar(int, int) = 0;
	virtual void unwrapmatchar(int, int) = 0;
	virtual void unwrapfloat(int, int) = 0;
	virtual void unwraprowfloat(int, int) = 0;
	virtual void unwrapmatfloat(int, int) = 0;
	virtual void unwraplong(int, int) = 0;
	virtual void unwraprowlong(int, int) = 0;
	virtual void unwrapmatlong(int, int) = 0;
};

#endif //__SCILABABSTRACTENVIRONEMNTWRAPPER_H__
