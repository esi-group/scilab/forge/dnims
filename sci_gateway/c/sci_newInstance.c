/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
#include "noMoreMemory.h"
/*--------------------------------------------------------------------------*/
int sci_newInstance(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int idClass = 0;
    int *tmpvar = NULL;
    int *args = NULL;
    int i = 0;
    int ret = 0;
    char *errmsg = NULL;
    char *className = NULL;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    if (Rhs == 0)
    {
        Scierror(999, "%s: Wrong number of arguments : more than 1 argument expected\n", fname);
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (isStringType(pvApiCtx, addr))
    {
        className = getSingleString(1, fname);
        if (!className)
        {
            return 0;
        }
        idClass = loadclass(fname, className, getAllowReload(), &errmsg);
        if (errmsg)
        {
            Scierror(999, ENVIRONMENTERROR, fname, errmsg);
            FREE(errmsg);
            freeAllocatedSingleString(className);
            return 0;
        }
    }
    else
    {
        idClass = getIdOfArg(addr, fname, NULL, 1, 1);
        if (idClass == -1)
        {
            return 0;
        }
    }

    tmpvar = (int*)MALLOC(sizeof(int) * Rhs);
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }

    *tmpvar = 0;
    args = (int*)MALLOC(sizeof(int) * (Rhs - 1));
    if (!args)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }

    for (i = 0; i < Rhs - 1; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i + 2, &addr);
        if (err.iErr)
        {
            removeTemporaryVars(fname, tmpvar);
            FREE(tmpvar);
            FREE(args);
            printError(&err, 0);
            return 0;
        }
        args[i] = getIdOfArg(addr, fname, tmpvar, 0, i + 2);
        if (args[i] == -1)
        {
            removeTemporaryVars(fname, tmpvar);
            FREE(tmpvar);
            FREE(args);
            return 0;
        }
    }

    ret = newinstance(fname, idClass, args, Rhs - 1, &errmsg);
    FREE(args);

    removeTemporaryVars(fname, tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
    {
        removescilabobject(fname, ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
