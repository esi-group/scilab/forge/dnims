/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SCILABOBJECTS_H__
#define __SCILABOBJECTS_H__
/*--------------------------------------------------------------------------*/
#ifdef _MSC_VER
#include <windows.h>
#endif

#ifndef _MSC_VER /* Defined anyway with Visual */
#if !defined(byte)
typedef signed char byte;
#else
#pragma message("Byte has been redefined elsewhere. Some problems can happen")
#endif
#endif
/* ------------------------------------------------------------------------- */
static int ZERO = 0;
static int ONE = 1;
static int TWO = 2;
static int THREE = 3;
/* ------------------------------------------------------------------------- */

#define __USE_DEPRECATED_STACK_FUNCTIONS__

#ifdef __cplusplus
extern "C"
{
#endif


void initialization(const char* fname);
void initializeenvironmentfactory();
int createNamedEnvironmentObject(int type, const char *name, int id, const char* fname);
int createNamedEnvironmentObjectEnv(int type, const char *name, int id, const int envId);
void copyInvocationMacroToStack(int pos);
int removeVar(const char *fname, int *addr, int pos, int isEnvOrClass);
int createEnvironmentObjectAtPos(int type, int pos, int id, const char* fname);
int createEnvironmentObjectAtPosEnv(int type, int pos, int id, const int envId);
void removeTemporaryVars(char *fname, int *tmpvar);
void removeTemporaryVarsEnv(const int envId, int *tmpvar);
void enabletrace(char*, char**);
void disabletrace();
void garbagecollect(const char*, char**);
int loadclass(const char*, char*, char, char**);
int createarray(const char*, char*, int*, int, char**);
int newinstance(const char*, int, int*, int, char**);
int newinstancenve(int, int, int*, int, char**);
int invoke(const char*, int, char*, int*, int, char**);
int invokeenv(int, int, char*, int*, int, char**);
void setfield(const char*, int, char*, int, char**);
void setfieldenv(int, int, char*, int, char**);
int getfield(const char*, int, char*, char**);
int getfieldenv(int, int, char*, char**);
int getfieldtype(const char*, int, char*, char**);
int getfieldtypeenv(int, int, char*, char**);
char* getclassname(const char*, int, char**);
int getarrayelement(const char*, int, int *, int, char**);
int getarrayelementenv(int, int, int *, int, char**);
void setarrayelement(const char*, int, int *, int, int, char**);
void setarrayelementenv(int, int, int *, int, int, char**);
int cast(const char*, int, char*, char**);
int castwithid(const char*, int, int, char**);
char* getrepresentation(char*, int, char**);
char* getrepresentationenv(int, int, char**);
int isvalidobject(char*, int);
void removescilabobject(const char*, int);
void removescilabobjectenv(const int envId, int id);
void getaccessiblemethods(const char*, int, int, char**);
void getaccessiblefields(const char*, int, int, char**);
int wrapSingleDouble(double, int);
int wrapRowDouble(double*, int, int);
int wrapMatDouble(double*, int, int, int);
int wrapSingleInt(int, int);
int wrapRowInt(int*, int, int);
int wrapMatInt(int*, int, int, int);
int wrapSingleUInt(unsigned int, int);
int wrapRowUInt(unsigned int*, int, int);
int wrapMatUInt(unsigned int*, int, int, int);
int wrapSingleByte(byte, int);
int wrapRowByte(byte*, int, int);
int wrapMatByte(byte*, int, int, int);
int wrapSingleUByte(unsigned char, int);
int wrapRowUByte(unsigned char*, int, int);
int wrapMatUByte(unsigned char*, int, int, int);
int wrapSingleShort(short, int);
int wrapRowShort(short*, int, int);
int wrapMatShort(short*, int, int, int);
int wrapSingleUShort(unsigned short, int);
int wrapRowUShort(unsigned short*, int, int);
int wrapMatUShort(unsigned short*, int, int, int);
int wrapSingleString(char*, int);
int wrapRowString(char**, int, int);
int wrapMatString(char**, int, int, int);
int wrapSingleBoolean(int, int);
int wrapRowBoolean(int*, int, int);
int wrapMatBoolean(int*, int, int, int);
int wrapSingleChar(unsigned short, int);
int wrapRowChar(unsigned short*, int, int);
int wrapMatChar(unsigned short*, int, int, int);
int wrapSingleFloat(double, int);
int wrapRowFloat(double*, int, int);
int wrapMatFloat(double*, int, int, int);

#ifdef __SCILAB_INT64__
int wrapSingleLong(long long);
int wrapRowLong(long long*, int);
int wrapMatLong(long long*, int, int);
#endif

void* wrapAsDirectDoubleBuffer(double*, long, int*);
void* wrapAsDirectLongBuffer(long*, long, int*);
void* wrapAsDirectIntBuffer(int*, long, int*);
void* wrapAsDirectShortBuffer(short*, long, int*);
void* wrapAsDirectByteBuffer(byte*, long, int*);
void* wrapAsDirectCharBuffer(char*, long, int*);
void* wrapAsDirectFloatBuffer(float*, long, int*);
void releasedirectbuffer(void**, int*, int, char**);

int compilecode(const char*, char*, char**, int, char**);

int unwrap(int idObj, int pos, const char *fname);
int unwrapenv(int idObj, int pos, const int);
void unwrapdouble(int, int, char**, const int);
void unwraprowdouble(int, int, char**, const int);
void unwrapmatdouble(int, int, char**, const int);
void unwrapbyte(int, int, char**, const int);
void unwraprowbyte(int, int, char**, const int);
void unwrapmatbyte(int, int, char**, const int);
void unwrapshort(int, int, char**, const int);
void unwraprowshort(int, int, char**, const int);
void unwrapmatshort(int, int, char**, const int);
void unwrapint(int, int, char**, const int);
void unwraprowint(int, int, char**, const int);
void unwrapmatint(int, int, char**, const int);
void unwrapboolean(int, int, char**, const int);
void unwraprowboolean(int, int, char**, const int);
void unwrapmatboolean(int, int, char**, const int);
void unwrapstring(int, int, char**, const int);
void unwraprowstring(int, int, char**, const int);
void unwrapmatstring(int, int, char**, const int);
void unwrapchar(int, int, char**, const int);
void unwraprowchar(int, int, char**, const int);
void unwrapmatchar(int, int, char**, const int);
void unwrapfloat(int, int, char**, const int);
void unwraprowfloat(int, int, char**, const int);
void unwrapmatfloat(int, int, char**, const int);
void unwraplong(int, int, char**, const int);
void unwraprowlong(int, int, char**, const int);
void unwrapmatlong(int, int, char**, const int);
int isunwrappable(const int, int, char**);

int getEnvironmentId(const char* fname);
void initializeenvironment(const char* fname, char** errmsg);

#if defined(WIN32) || defined(WIN64)
void dotnetload(const char* fname, const char* longName, int allowReload, char** errmsg);
#endif

char* getenvironmentname(int environmentId);

#ifdef __cplusplus
}; 
#endif /* extern "C" */
/*--------------------------------------------------------------------------*/
#endif /* __SCILABOBJECTS_H__ */
