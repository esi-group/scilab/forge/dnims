/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "getSciArgs.h"
#include "api_scilab.h"
#include "stack-c.h"
#include "MALLOC.h"
#include "ScilabObjects.h"
#include "Scierror.h"
/*--------------------------------------------------------------------------*/
#define WRAP(javatype,data)                                             \
    if (row == 0 || col == 0)                                           \
    {                                                                   \
        returnId = 0;                                                   \
    }                                                                   \
    else if (row == 1 && col == 1)                                      \
    {                                                                   \
        returnId = wrapSingle ## javatype(data[0], envId);              \
    }                                                                   \
    else if (row == 1)                                                  \
    {                                                                   \
        returnId = wrapRow ## javatype(data, col, envId);               \
    }                                                                   \
    else                                                                \
    {                                                                   \
        returnId = wrapMat ## javatype(data, row, col, envId);          \
    }                                                                   \
/*--------------------------------------------------------------------------*/
#define CHECKANDRETURN(x) if (err.iErr)         \
    {                                           \
        printError(&err, 0);                    \
        return -1;                              \
    }                                           \
    return (int) (*x)
/*--------------------------------------------------------------------------*/
static int zero = 0;
/*--------------------------------------------------------------------------*/
int compareStrToMlistType(char *str, int *mlist)
{
    char **mlist_type = NULL;
    int i = 0, type;
    int rows, cols;
    int *lengths = NULL;
    int cmp;

    SciErr err = getVarType(pvApiCtx, mlist, &type);
    if (err.iErr || type != sci_mlist)
    {
        return 0;
    }

    err = getMatrixOfStringInList(pvApiCtx, mlist, 1, &rows, &cols, NULL, NULL);
    if (err.iErr || rows != 1 || cols <= 0)
    {
        return 0;
    }

    lengths = (int*)MALLOC(sizeof(int) * rows * cols);
    err = getMatrixOfStringInList(pvApiCtx, mlist, 1, &rows, &cols, lengths, NULL);
    if (err.iErr)
    {
        return 0;
    }

    mlist_type = (char*)MALLOC(sizeof(char*) * rows * cols);
    for (; i < rows * cols; i++)
    {
        mlist_type[i] = (char*)MALLOC(sizeof(char) * (lengths[i] + 1));
    }

    err = getMatrixOfStringInList(pvApiCtx, mlist, 1, &rows, &cols, lengths, mlist_type);
    if (err.iErr)
    {
        return 0;
    }

    cmp = strcmp(mlist_type[0], str);
    freeAllocatedMatrixOfString(rows, cols, mlist_type);
    FREE(lengths);

    return !cmp;
}
/*--------------------------------------------------------------------------*/
int isEnvObj(int *mlist)
{
    return compareStrToMlistType("_EObj", mlist);
}
/*--------------------------------------------------------------------------*/
int isEnvClass(int *mlist)
{
    return compareStrToMlistType("_EClass", mlist);
}
/*--------------------------------------------------------------------------*/
int isEnvVoid(int *mlist)
{
    return compareStrToMlistType("_EVoid", mlist);
}
/*--------------------------------------------------------------------------*/
/**
 * Get a single string on the stack or return NULL if an error occured
 */
char* getSingleString(int pos, const char *fname)
{
    SciErr err;
    int *addr = NULL;
    int typ = 0, ret = 0;
    char *str = NULL;

    err = getVarAddressFromPosition(pvApiCtx, pos, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return NULL;
    }

    if (!isStringType(pvApiCtx, addr))
    {
        Scierror(999, "%s: Wrong type for input argument %i: String expected\n", fname, pos);
        return NULL;
    }

    if (!isScalar(pvApiCtx, addr))
    {
        Scierror(999, "%s: Wrong size for input argument %i: String expected\n", fname, pos);
        return NULL;
    }

    ret = getAllocatedSingleString(pvApiCtx, addr, &str);
    if (ret)
    {
        freeAllocatedSingleString(str);
        return NULL;
    }

    return str;
}
/*--------------------------------------------------------------------------*/
int isPositiveIntegerAtAddress(int *addr)
{
    SciErr err;
    int typ = 0, row, col, prec;
    int *id = NULL;

    err = getVarDimension(pvApiCtx, addr, &row, &col);
    if (err.iErr)
    {
        printError(&err, 0);
        return -1;
    }

    if (row != 1 || col != 1)
    {
        return -1;
    }

    err = getVarType(pvApiCtx, addr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return -1;
    }

    if (typ == sci_ints)
    {
        err = getMatrixOfIntegerPrecision(pvApiCtx, addr, &prec);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }
        switch (prec)
        {
        case SCI_INT8:
        {
            char *cvalue = NULL;
            err = getMatrixOfInteger8(pvApiCtx, addr, &row, &col, &cvalue);
            CHECKANDRETURN(cvalue);
        }
        break;
        case SCI_UINT8:
        {
            unsigned char *ucvalue = NULL;
            err = getMatrixOfUnsignedInteger8(pvApiCtx, addr, &row, &col, &ucvalue);
            CHECKANDRETURN(ucvalue);
        }
        break;
        case SCI_INT16:
        {
            short *svalue = NULL;
            err = getMatrixOfInteger16(pvApiCtx, addr, &row, &col, &svalue);
            CHECKANDRETURN(svalue);
        }
        break;
        case SCI_UINT16:
        {
            unsigned short *usvalue = NULL;
            err = getMatrixOfUnsignedInteger16(pvApiCtx, addr, &row, &col, &usvalue);
            CHECKANDRETURN(usvalue);
        }
        break;
        case SCI_INT32:
        {
            int *ivalue = NULL;
            err = getMatrixOfInteger32(pvApiCtx, addr, &row, &col, &ivalue);
            CHECKANDRETURN(ivalue);
        }
        break;
        case SCI_UINT32:
        {
            unsigned int *uivalue = NULL;
            err = getMatrixOfUnsignedInteger32(pvApiCtx, addr, &row, &col, &uivalue);
            CHECKANDRETURN(uivalue);
        }
        break;
#ifdef __SCILAB_INT64__
        case SCI_INT64:
        {
            long long *llvalue = NULL;
            err = getMatrixOfInteger64(pvApiCtx, addr, &row, &col, &llvalue);
            CHECKANDRETURN(llvalue);
        }
        break;
        case SCI_UINT64:
        {
            unsigned long long *ullvalue = NULL;
            err = getMatrixOfUnsignedInteger64(pvApiCtx, addr, &row, &col, &ullvalue);
            CHECKANDRETURN(ullvalue);
        }
        break;
#endif
        default:
            return -1;
        }
    }
    else if (typ == sci_matrix)
    {
        double *dvalue = NULL;

        if (isVarComplex(pvApiCtx, addr))
        {
            return -1;
        }

        err = getMatrixOfDouble(pvApiCtx, addr, &row, &col, &dvalue);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }

        if (*dvalue - (double)(int)(*dvalue) == 0.0)
        {
            return (int)(*dvalue);
        }
    }

    return -1;
}
/*--------------------------------------------------------------------------*/
int getIdOfArgEnv(int *addr, const char *fname, int *tmpvars, char isClass, int pos, int envId)
{
    SciErr err;
    int typ, row = 0, col = 0, returnId;

    err = getVarType(pvApiCtx, addr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return -1;
    }

    if (isClass && typ != sci_mlist)
    {
        Scierror(999, "%s: Wrong type for input argument %i: _EClass is expected\n", fname, pos);
        return -1;
    }

    switch (typ)
    {
    case sci_matrix :
    {
        double *mat = NULL;

        if (isVarComplex(pvApiCtx, addr))
        {
            Scierror(999,"%s: Wrong type for input argument %i: Complex are not handled\n", fname, pos);
            return -1;
        }

        err = getMatrixOfDouble(pvApiCtx, addr, &row, &col, &mat);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }

        WRAP(Double, mat);

        tmpvars[++tmpvars[0]] = returnId;

        return returnId;
    }
    case sci_ints :
    {
        int prec = 0;
        void *ints = NULL;

        err = getMatrixOfIntegerPrecision(pvApiCtx, addr, &prec);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }

        switch (prec)
        {
        case SCI_INT8 :;
            err = getMatrixOfInteger8(pvApiCtx, addr, &row, &col, (char**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(Byte, ((byte*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_UINT8 :;
            err = getMatrixOfUnsignedInteger8(pvApiCtx, addr, &row, &col, (unsigned char**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(UByte, ((unsigned char*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_INT16 :;
            err = getMatrixOfInteger16(pvApiCtx, addr, &row, &col, (short**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(Short, ((short*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_UINT16 :;
            err = getMatrixOfUnsignedInteger16(pvApiCtx, addr, &row, &col, (unsigned short**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(UShort, ((unsigned short*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_INT32 :;
            err = getMatrixOfInteger32(pvApiCtx, addr, &row, &col, (int**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(Int, ((int*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_UINT32 :;
            err = getMatrixOfUnsignedInteger32(pvApiCtx, addr, &row, &col, (unsigned int**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(UInt, ((unsigned int*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;

#ifdef __SCILAB_INT64__
        case SCI_INT64 :;
            err = getMatrixOfInteger64(pvApiCtx, addr, &row, &col, (long long**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(Long, ((long long*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
        case SCI_UINT64 :;
            err = getMatrixOfUnsignedInteger64(pvApiCtx, addr, &row, &col, (unsigned long long**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            WRAP(Long, ((long long*)ints));
            tmpvars[++tmpvars[0]] = returnId;
            return returnId;
#endif
        }
    }
    case sci_strings :
    {
        char **matS = NULL;
        if (getAllocatedMatrixOfString(pvApiCtx, addr, &row, &col, &matS))
        {
            return -1;
        }

        WRAP(String, matS);
        freeAllocatedMatrixOfString(row, col, matS);
        tmpvars[++tmpvars[0]] = returnId;

        return returnId;
    }
    case sci_boolean :
    {
        int *matB;

        err = getMatrixOfBoolean(pvApiCtx, addr, &row, &col, &matB);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }
        WRAP(Boolean, matB);
        tmpvars[++tmpvars[0]] = returnId;

        return returnId;
    }
    case sci_mlist :
    {
        int *id = 0;
        int *environmentId = 0;
        int jc = isEnvClass(addr);

        if (isClass)
        {
            if (jc)
            {
                err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
                if (err.iErr)
                {
                    printError(&err, 0);
                    return -1;
                }
                return *id;
            }
            else
            {
                Scierror(999,"%s: Wrong type for input argument %i: _EClass is expected\n", fname, pos);
                return -1;
            }
        }

        if (isEnvObj(addr) || jc)
        {
            err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            return *id;
        }
        else
        {
            Scierror(999, "%s: Wrong type for input argument %i: _EClass or _EObj are expected\n", fname, pos);
            return -1;
        }

        break;
    }
    default :;
        Scierror(999, "%s: Wrong type for input argument %i: Cannot wrap it\n", fname, pos);
        return -1;
    }
}
/*--------------------------------------------------------------------------*/
int getIdOfArg(int *addr, const char *fname, int *tmpvars, char isClass, int pos)
{
    return getIdOfArgEnv(addr, fname, tmpvars, isClass, pos, getEnvironmentId(fname));
}
/*--------------------------------------------------------------------------*/
int getIdOfArgAsDirectBuffer(int pos, const char *fname, char forceByteBuffer, void **tmpref)
{
    SciErr err;
    int typ, row = 0, col = 0, returnId;
    int *addr = NULL;
    char *varName = NULL;

    varName = getSingleString(pos, fname);
    if (!varName)
    {
        return -1;
    }

    err = getVarAddressFromName(pvApiCtx, varName, &addr);
    FREE(varName);
    if (err.iErr)
    {
        printError(&err, 0);
        return -1;
    }

    err = getVarType(pvApiCtx, addr, &typ);
    if (err.iErr)
    {
        printError(&err, 0);
        return -1;
    }

    switch (typ)
    {
    case sci_matrix :
    {
        double *mat = NULL;
        if (isVarComplex(pvApiCtx, addr))
        {
            Scierror(999,"%s: Wrong type for input argument %s: Complex are not handled\n", fname, varName);
            return -1;
        }

        err = getMatrixOfDouble(pvApiCtx, addr, &row, &col, &mat);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }
        if (forceByteBuffer)
        {
            *tmpref = wrapAsDirectByteBuffer((byte*)mat, sizeof(double) * row * col, &returnId);
        }
        else
        {
            *tmpref = wrapAsDirectDoubleBuffer(mat, sizeof(double) * row * col, &returnId);
        }

        return returnId;
    }
    case sci_ints :
    {
        int prec = 0;
        void *ints = NULL;

        err = getMatrixOfIntegerPrecision(pvApiCtx, addr, &prec);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }

        switch (prec)
        {
        case SCI_INT8 :
        {
            err = getMatrixOfInteger8(pvApiCtx, addr, &row, &col, (char**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(byte) * row * col, &returnId);
            return returnId;
        }
        case SCI_UINT8 :
        {
            err = getMatrixOfUnsignedInteger8(pvApiCtx, addr, &row, &col, (unsigned char**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(byte) * row * col, &returnId);
            return returnId;
        }
        case SCI_INT16 :
        {
            err = getMatrixOfInteger16(pvApiCtx, addr, &row, &col, (short**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(short) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectShortBuffer((short*)ints, sizeof(short) * row * col, &returnId);
            }
            return returnId;
        }
        case SCI_UINT16 :
        {
            err = getMatrixOfUnsignedInteger16(pvApiCtx, addr, &row, &col, (unsigned short**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(short) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectShortBuffer((short*)ints, sizeof(short) * row * col, &returnId);
            }
            return returnId;
        }
        case SCI_INT32 :
        {
            err = getMatrixOfInteger32(pvApiCtx, addr, &row, &col, (int**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(int) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectIntBuffer((int*)ints, sizeof(int) * row * col, &returnId);
            }
            return returnId;
        }
        case SCI_UINT32 :
        {
            err = getMatrixOfUnsignedInteger32(pvApiCtx, addr, &row, &col, (unsigned int**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(int) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectIntBuffer((int*)ints, sizeof(int) * row * col, &returnId);
            }
            return returnId;
        }
#ifdef __SCILAB_INT64__
        case SCI_INT64 :
        {
            err = getMatrixOfInteger64(pvApiCtx, addr, &row, &col, (long long**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)mat, sizeof(long long) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectLongBuffer((long long*)ints, sizeof(long long) * row * col, &returnId);
            }
            return returnId;
        }
        case SCI_UINT64 :
        {
            err = getMatrixOfUnsignedInteger64(pvApiCtx, addr, &row, &col, (unsigned long long**)(&ints));
            if (err.iErr)
            {
                printError(&err, 0);
                return -1;
            }
            if (forceByteBuffer)
            {
                *tmpref = wrapAsDirectByteBuffer((byte*)mat, sizeof(long long) * row * col, &returnId);
            }
            else
            {
                *tmpref = wrapAsDirectLongBuffer((long long*)ints, sizeof(long long) * row * col, &returnId);
            }
            return returnId;
        }
#endif
        }
    case sci_strings :
    {
        Scierror(999,"%s: Wrong type for input argument %s: String not supported\n", fname, varName);
        return -1;
    }
    case sci_boolean :
    {
        int *matB = NULL;
        err = getMatrixOfBoolean(pvApiCtx, addr, &row, &col, &matB);
        if (err.iErr)
        {
            printError(&err, 0);
            return -1;
        }
        if (forceByteBuffer)
        {
            *tmpref = wrapAsDirectByteBuffer((byte*)ints, sizeof(int) * row * col, &returnId);
        }
        else
        {
            *tmpref = wrapAsDirectIntBuffer((int*)ints, sizeof(int) * row * col, &returnId);
        }
        return returnId;
    }
    default :
    {
        Scierror(999, "%s: Wrong type for input argument %s: type not supported\n", fname, varName);
        return -1;
    }

    }
    }
}
/*--------------------------------------------------------------------------*/
int getIdOfEnvironemnt(int *addr, const char *fname, char isClass, int pos)
{
	SciErr err;
	int typ, row = 0, col = 0, returnId;
	int *environmentId = 0;
	int jc = isEnvClass(addr);

	err = getVarType(pvApiCtx, addr, &typ);
	if (err.iErr)
	{
		printError(&err, 0);
		return -1;
	}

	if (isClass && typ != sci_mlist)
	{
		Scierror(999, "%s: Wrong type for input argument %i: _EClass is expected\n", fname, pos);
		return -1;
	}

	if (isClass)
	{
		if (jc)
		{
			err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &environmentId);
			if (err.iErr)
			{
				printError(&err, 0);
				return -1;
			}
			return *environmentId;
		}
		else
		{
			Scierror(999,"%s: Wrong type for input argument %i: _EClass is expected\n", fname, pos);
			return -1;
		}
	}

	if (isEnvObj(addr) || jc)
	{
		err = getMatrixOfInteger32InList(pvApiCtx, addr, 2, &row, &col, &environmentId);
		if (err.iErr)
		{
			printError(&err, 0);
			return -1;
		}
		return *environmentId;
	}
	else
	{
		Scierror(999, "%s: Wrong type for input argument %i: _EClass or _EObj are expected\n", fname, pos);
		return -1;
	}
}
/*--------------------------------------------------------------------------*/
