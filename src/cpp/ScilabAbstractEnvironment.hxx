#ifndef __SCILABABSTRACTENVIRNOMENT_H__
#define __SCILABABSTRACTENVIRNOMENT_H__

#include "ScilabAbstractEnvironmentException.hxx"

class ScilabAbstractEnvironment
{
public:
	virtual void garbagecollect() = 0;

	virtual int createarray(char *className, int* dims, int len) = 0;

	virtual int loadclass(char *className, char allowReload) = 0;

	virtual char* getrepresentation(int id) = 0;

	virtual int isvalidobject(int id) = 0;

	virtual int newinstance(int id, int *args, int argsSize) = 0;

	virtual int invoke(int id, char *methodName, int *args, int argsSize) = 0;

	virtual void setfield(int id, char *fieldName, int idarg) = 0;

	virtual int getfield(int id, char *fieldName) = 0;

	virtual int getfieldtype(int id, char *fieldName) = 0;

	virtual int getarrayelement(int id, int *index, int length) = 0;

	virtual void setarrayelement(int id, int *index, int length, int idArg) = 0;
	
	virtual int cast(int id, char *objName) = 0;
	
	virtual int castwithid(int id, int classId) = 0;
		
	virtual void removescilabobject(int id) = 0;
		
	virtual void getaccessiblemethods(int id, int pos) = 0;

	virtual void getaccessiblefields(int id, int pos) = 0;

	virtual char* getclassname(int id) = 0;

	virtual int isunwrappable(int id) = 0;

	virtual int compilecode(char *className, char **code, int size) = 0;
};

#endif //__SCILABABSTRACTENVIRNOMENT_H__
