#include "ScilabDotNetType.h"
#include <iostream>

ScilabDotNetType::ScilabDotNetType(Type* aType)
{
    methodSignatures_ = new List<String*>();
    methodNames_ = new List<String*>();
    fieldNames_ = new List<String*>();
    staticMethodSignatures_ = new List<String*>();
    staticMethodNames_ = new List<String*>();
    staticFieldNames_ = new List<String*>();
    
    MethodInfo* minfs[] = aType->GetMethods();    

    for (int i=0;i<minfs->Length;++i)
    {
        methodSignatures_->Add(minfs[i]->ToString());
        methodNames_->Add(minfs[i]->Name);

        if (minfs[i]->IsStatic)
        {
            staticMethodSignatures_->Add(minfs[i]->ToString());
            staticMethodNames_->Add(minfs[i]->Name);
        }        
    }    

    FieldInfo* fields[] = aType->GetFields();
    for (int i=0;i<fields->Length;++i)
    {
        fieldNames_->Add(fields[i]->Name);        
        if(fields[i]->IsStatic)
        {
            staticFieldNames_->Add(fields[i]->Name);
        }
    }

    PropertyInfo* properties[] = aType->GetProperties();

    for (int i=0;i<properties->Length;++i)
    {
        fieldNames_->Add(properties[i]->Name);
        if (properties[i]->GetGetMethod()->IsStatic)
        {
            staticFieldNames_->Add(properties[i]->Name);
        }        
    }

    type_ = aType;
}

bool ScilabDotNetType::HasMethod(String* methodName)
{
    return methodNames_->Contains(methodName);    
}

bool ScilabDotNetType::HasField(String* fieldName)
{
    return fieldNames_->Contains(fieldName);    
}

List<String*>* ScilabDotNetType::GetAccessibleMethodNames()
{
    return methodSignatures_;
}

List<String*>* ScilabDotNetType::GetAccessibleStaticMethodNames()
{
    return staticMethodNames_;
}

List<String*>* ScilabDotNetType::GetAccessibleFieldNames()
{
    return fieldNames_;
}

List<String*>* ScilabDotNetType::GetAccessibleStaticFieldNames()
{
    return staticFieldNames_;
}