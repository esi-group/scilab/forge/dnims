/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
int sci_unwrap(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int row = 0, col = 0;
    int *id = NULL;
    int i = 1;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    if (Rhs == 0)
    {
        Scierror(999, "%s: One or more argument expected\n", fname);
        return 0;
    }

    CheckLhs(Rhs, Rhs);

    for (; i <= Rhs; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        if (!isEnvObj(addr))
        {
            Scierror(999, "%s: Wrong type for argument %d : _EObj expected\n", fname, i);
            return 0;
        }

        err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
        if (err.iErr)
        {
            printError(&err, 0);
            return 0;
        }

        if (!unwrap(*id, Rhs + i, fname) && !createEnvironmentObjectAtPos(_ENVOBJ, Rhs + i, *id, fname))
        {
            return 0;
        }

        LhsVar(i) = Rhs + i;
    }
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
