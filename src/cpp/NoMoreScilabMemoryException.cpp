/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "NoMoreScilabMemoryException.hxx"
/*--------------------------------------------------------------------------*/
namespace ScilabObjects {

    NoMoreScilabMemoryException::NoMoreScilabMemoryException(void) throw() : exception() { }

    NoMoreScilabMemoryException::~NoMoreScilabMemoryException(void) throw() { }

    /**
     * @return a description of the exception
     */
    const char * NoMoreScilabMemoryException::what(void) const throw()
    {
        return NOMOREMEMORY;
    }
}
/*--------------------------------------------------------------------------*/
