//
//  Copyright (C) 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

jimport java.lang.Double;
d = Double.new("1.23456");
jremove d;
if execstr("jgetfields(d)", "errcatch") <> 999 then pause, end
if lasterror() <> msprintf(_("%s: %s"), "getFields", "Invalid Java object") then pause, end

