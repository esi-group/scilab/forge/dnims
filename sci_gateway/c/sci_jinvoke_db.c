/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "noMoreMemory.h"
/*--------------------------------------------------------------------------*/
/* Function invoke is called with more than 2 arguments : invoke(obj,method,arg1,...,argn)
   - obj is a _JObj mlist so we get his id or can be a String or a number. In this last case, the obj is converted
   in a Java object and the id is got.
   - method is the name of the method.
   - arg1,...,argn are the arguments of the called method, if they're not _JObj mlist, they're converted when it is possible
*/
int sci_jinvoke_db(char *fname)
{
    SciErr err;
    char *errmsg = NULL;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int row = 0, col = 0;
    int idObj = 0;
    int *args = NULL;
    void **tmpref = NULL, **stmpref = NULL;
    int i = 0;
    char *methName = NULL;
    int ret = 0;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    if (Rhs < 2)
    {
        Scierror(999, "%s: Wrong number of arguments : more than 2 arguments expected\n", fname);
        return 0;
    }

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        FREE(tmpvar);
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

    args = (int*)MALLOC(sizeof(int) * (Rhs - 2));
    if (!args)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }

    tmpref = (void**)MALLOC(sizeof(void*) * (Rhs - 2));
    if (!tmpref)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return NULL;
    }
    stmpref = tmpref;

    for (; i < Rhs - 2; i++)
    {
        args[i] = getIdOfArgAsDirectBuffer(i + 3, fname, 0, tmpref);
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            removeTemporaryVars(fname, tmpvar);
            releasedirectbuffer(stmpref, args, i, &errmsg);
            FREE(args);
            FREE(stmpref);
            return 0;
        }
        tmpref++;
    }

    methName = getSingleString(2, fname);
    if (!methName)
    {
        removeTemporaryVars(fname, tmpvar);
        releasedirectbuffer(stmpref, args, Rhs - 2, &errmsg);
        FREE(args);
        FREE(stmpref);
        return 0;
    }

    ret = invoke(fname, idObj, methName, args, Rhs - 2, &errmsg);
    freeAllocatedSingleString(methName);
    removeTemporaryVars(fname, tmpvar);
    releasedirectbuffer(stmpref, args, Rhs - 2, &errmsg);
    FREE(args);
    FREE(stmpref);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
    {
        removescilabobject(fname, ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
