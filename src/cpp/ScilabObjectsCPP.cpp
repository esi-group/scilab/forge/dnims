/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/* ------------------------------------------------------------------------- */
#include <string.h>
#include "OptionsHelper.h"
#include "ScilabObjects.hxx"
#include "ScilabClassLoader.hxx"
#include "ScilabJavaClass.hxx"
#include "ScilabJavaObjectBis.hxx"
#include "ScilabJavaArray.hxx"
#include "ScilabJavaCompiler.hxx"
#include "GiwsException.hxx"
#include "NoMoreScilabMemoryException.hxx"
#include "wrap.hpp"
#include "wrapwithcast.hpp"
#include "unwrap.hpp"
#include "WrapAsDirectBufferTemplate.hpp"
#include "ScilabAbstractEnvironment.hxx"
#include "ScilabJavaEnvironment.hxx"
#if defined(WIN32) || defined(WIN64)
#include "ScilabDotNetEnvironment.hxx"
#endif
#include "ScilabAbstractEnvironmentFactory.hxx"
/* ------------------------------------------------------------------------- */
using namespace ScilabObjects;
/* ------------------------------------------------------------------------- */
/*
 * Syntax: wrapForType(type, Type)
 * type: the Scilab data type
 * Type: used for the function name
 */
wrapForType(double,Double)
wrapForType(int,Int)
wrapForType(byte,Byte)
wrapForType(short,Short)
wrapForType(unsigned short,Char)

#ifdef __SCILAB_INT64__
wrapForType(long long,Long)
#endif
/* ------------------------------------------------------------------------- */
/*
 * Syntax: wrapWithCastForType(type, castType, Type)
 * type: the Scilab data type
 * castType: datas are copied and casted (char *fname, into castType)
 * Type: used for the function name
 */
wrapWithCastForType(unsigned int, long long, UInt)
wrapWithCastForType(unsigned char, short, UByte)
wrapWithCastForType(unsigned short, int, UShort)
wrapWithCastForType(int, bool, Boolean)
wrapWithCastForType(double, float, Float)
/* ------------------------------------------------------------------------- */
/*
 * Syntax: unwrapForType(type,ScilabType,JNIType,Type)
 * type: used for the function name (unwrap##type(...))
 * ScilabType: the type in Scilab
 * JNIType: the type in JNI
 * Type: used for the functions wrapper
 */
unwrapForType(double,double,jdouble,Double)
unwrapForType(int,int,jint,Int)
unwrapForType(boolean,int,jboolean,Boolean)
unwrapForType(byte,char,jbyte,Byte)
unwrapForType(short,short,jshort,Short)
unwrapForType(char,unsigned short,jchar,Char)
unwrapForType(long,long,jlong,Long)
unwrapForType(float,double,jfloat,Float)
/* ------------------------------------------------------------------------- */

wrapIntoDirectBuffer(Double,double)
wrapIntoDirectBuffer(Int,int)
wrapIntoDirectBuffer(Char,char)
wrapIntoDirectBuffer(Short,short)
wrapIntoDirectBuffer(Long,long)
wrapIntoDirectBuffer(Byte,byte)
wrapIntoDirectBuffer(Float,float)
/* ------------------------------------------------------------------------- */
void unwrapstring(int id, int pos, char **errmsg, const int envId)
{
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);
		abstrEnv->unwrapstring(id, pos);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}	
}
/* ------------------------------------------------------------------------- */
void unwraprowstring(int id, int pos, char **errmsg, const int envId)
{
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);
		abstrEnv->unwraprowstring(id, pos);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}		
}
/* ------------------------------------------------------------------------- */
void unwrapmatstring(int id, int pos, char **errmsg, const int envId)
{
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);
		abstrEnv->unwrapmatstring(id, pos);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}
}
/* ------------------------------------------------------------------------- */
void garbagecollect(const char *fname, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		abstrEnv->garbagecollect();
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}
}
/* ------------------------------------------------------------------------- */
int createarray(const char *fname, char *className, int* dims, int len, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->createarray(className, dims, len);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{			
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int loadclass(const char *fname, char *className, char allowReload, char **errmsg)
{		
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->loadclass(className, allowReload);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
char* getrepresentation(char *fname, int id, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->getrepresentation(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return NULL;
	}
}
/* ------------------------------------------------------------------------- */
char* getrepresentationenv(int envId, int id, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->getrepresentation(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return NULL;
	}
}
/* ------------------------------------------------------------------------- */
int isvalidobject(char *fname, int id)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->isvalidobject(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{		
		return 0;
	}   
}
/* ------------------------------------------------------------------------- */
int newinstance(const char *fname, int id, int *args, int argsSize, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->newinstance(id, args, argsSize);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int newinstancenve(int envId, int id, int *args, int argsSize, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->newinstance(id, args, argsSize);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int invoke(const char *fname, int id, char *methodName, int *args, int argsSize, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->invoke(id, methodName, args, argsSize);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int invokeenv(int envId, int id, char *methodName, int *args, int argsSize, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->invoke(id, methodName, args, argsSize);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
void setfield(const char *fname, int id, char *fieldName, int idarg, char **errmsg)
{
	setfieldenv(getEnvironmentId(fname), id, fieldName, idarg, errmsg);
}
/* ------------------------------------------------------------------------- */
void setfieldenv(int envId, int id, char *fieldName, int idarg, char **errmsg)
{	
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);    
		abstrEnv->setfield(id, fieldName, idarg);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return;
	}
}
/* ------------------------------------------------------------------------- */
int getfield(const char *fname, int id, char *fieldName, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->getfield(id, fieldName);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int getfieldenv(const int envId, int id, char *fieldName, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->getfield(id, fieldName);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int getfieldtype(const char *fname, int id, char *fieldName, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->getfieldtype(id, fieldName);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int getfieldtypeenv(int envId, int id, char *fieldName, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->getfieldtype(id, fieldName);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int getarrayelement(const char *fname, int id, int *index, int length, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->getarrayelement(id, index, length);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int getarrayelementenv(int envId, int id, int *index, int length, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->getarrayelement(id, index, length);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
void setarrayelement(const char *fname, int id, int *index, int length, int idArg, char **errmsg)
{
	setarrayelementenv(getEnvironmentId(fname), id, index, length, idArg, errmsg);
}
/* ------------------------------------------------------------------------- */
void setarrayelementenv(int  envId, int id, int *index, int length, int idArg, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);    
		abstrEnv->setarrayelement(id, index, length, idArg);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return;
	}
}
/* ------------------------------------------------------------------------- */
int cast(const char *fname, int id, char *objName, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->cast(id, objName);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int castwithid(const char *fname, int id, int classId, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->castwithid(id, classId);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
void removescilabobject(const char *fname, int id)
{	
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		abstrEnv->removescilabobject(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{		
		return;
	}
}
/* ------------------------------------------------------------------------- */
void removescilabobjectenv(const int envId, int id)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		abstrEnv->removescilabobject(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		return;
	}
}
/* ------------------------------------------------------------------------- */
void getaccessiblemethods(const char *fname, int id, int pos, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);    
		abstrEnv->getaccessiblemethods(id, pos);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}
}
/* ------------------------------------------------------------------------- */
void getaccessiblefields(const char *fname, int id, int pos, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);    
		abstrEnv->getaccessiblefields(id, pos);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
	}		
}
/* ------------------------------------------------------------------------- */
char* getclassname(const char *fname, int id, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->getclassname(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return NULL;
	}
}
/* ------------------------------------------------------------------------- */
int wrapSingleString(char *x, int envId)
{	
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);	
		return abstrEnv->wrapString(x);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{		
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int wrapRowString(char **x, int len, int envId)
{
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);	
		return abstrEnv->wrapString(x, len);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{		
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int wrapMatString(char **x, int r, int c, int envId)
{
	try
	{
		ScilabAbstractEnvironmentWrapper* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentWrapper((EnvironmentType)envId);

		if (getMethodOfConv())
		{
			char ***xx = new char**[r];
			int i, j, k;
			for (i = 0; i < r; i++)
			{
				xx[i] = new char*[c];
				for (j = 0; j < c; j++)
				{
					int len = strlen(x[j * r + i]) + 1;
					xx[i][j] = new char[len];
					memcpy(xx[i][j], x[j * r + i], len);
				}
			}
			k = abstrEnv->wrapString(xx, r, c);
			for (i = 0; i < r; i++)
			{
				for (j = 0; j < c; j++)
				{
					delete [] xx[i][j];
				}
				delete [] xx[i];
			}
			delete [] xx;
			return k;
		}
		else
		{
			char ***xx = new char**[c];
			int i;
			xx[0] = x;
			for (i = 1; i < c; xx[i] = xx[i++ - 1] + r);
			i = abstrEnv->wrapString(xx, c, r);
			delete [] xx;
			return i;
		}
	}
	catch(ScilabAbstractEnvironmentException& e)
	{		
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int isunwrappable(const int envId, int id, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironment((EnvironmentType)envId);
		return abstrEnv->isunwrappable(id);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
int compilecode(const char *fname, char *className, char **code, int size, char **errmsg)
{
	try
	{
		ScilabAbstractEnvironment* abstrEnv = ScilabAbstractEnvironmentFactory::getEnvironmentByMacroName(fname);
		return abstrEnv->compilecode(className, code, size);
	}
	catch(ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
void releasedirectbuffer(void **ref, int* id, int len, char **errmsg)
{
	try
    {
		ScilabJavaEnvironment* javaEnv = dynamic_cast<ScilabJavaEnvironment*>(ScilabAbstractEnvironmentFactory::getEnvironment(JAVA_ENVIRONMENT));
        javaEnv->releasedirectbuffer(ref, id, len);
    }
    catch (ScilabAbstractEnvironmentException& e)
    {
        *errmsg = strdup(e.GetDescription().c_str());
    }    
}
/* ------------------------------------------------------------------------- */
void enabletrace(char *filename, char **errmsg)
{
	try
    {
		ScilabJavaEnvironment* javaEnv = dynamic_cast<ScilabJavaEnvironment*>(ScilabAbstractEnvironmentFactory::getEnvironment(JAVA_ENVIRONMENT));   
        javaEnv->enabletrace(filename);
    }
    catch (ScilabAbstractEnvironmentException& e)
    {
        *errmsg = strdup(e.GetDescription().c_str());
    }  
}
/* ------------------------------------------------------------------------- */
void disabletrace()
{
	try
    {
		ScilabJavaEnvironment* javaEnv = dynamic_cast<ScilabJavaEnvironment*>(ScilabAbstractEnvironmentFactory::getEnvironment(JAVA_ENVIRONMENT));   
        javaEnv->disabletrace();
    }
    catch (ScilabAbstractEnvironmentException& e)
    {
        return;
    }
}
/* ------------------------------------------------------------------------- */
int getEnvironmentId(const char* fname)
{
	try
	{
		return ScilabAbstractEnvironmentFactory::getEnvironmentId(fname);
	}
	catch (ScilabAbstractEnvironmentException& e)
	{
		return -1;
	}
}
/* ------------------------------------------------------------------------- */
void initializeenvironment(const char* fname, char** errmsg)
{
	try
	{
		return ScilabAbstractEnvironmentFactory::initializeEnvironment(fname);
	}
	catch (ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return;
	}
}
/* ------------------------------------------------------------------------- */
void initializeenvironmentfactory()
{
	ScilabAbstractEnvironmentFactory::Initialize();
}
/* ------------------------------------------------------------------------- */
#if defined(WIN32) || defined(WIN64)
void dotnetload(const char* fname, const char* longName, int allowReload, char** errmsg)
{
	try
	{
		ScilabDotNetEnvironment* dNEnv = static_cast<ScilabDotNetEnvironment*>(ScilabAbstractEnvironmentFactory::getEnvironment(DOTNET_ENVIRONMENT));
		if (!strcmp(fname, "dnload"))
		{
			dNEnv->load(longName, allowReload);
		}
		else
		{
			dNEnv->loadfrom(longName, allowReload);
		}
	}
	catch (ScilabAbstractEnvironmentException& e)
	{
		*errmsg = strdup(e.GetDescription().c_str());
		return;
	}
}
#endif
/* ------------------------------------------------------------------------- */
char* getenvironmentname(int environmentId)
{
	return ScilabAbstractEnvironmentFactory::getEnvironmentName((EnvironmentType)environmentId);
}
