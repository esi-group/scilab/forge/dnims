function demo_swing()

  jimport java.awt.BorderLayout;
  jimport java.net.URL;
  jimport javax.imageio.ImageIO;
  jimport javax.swing.ImageIcon;
  jimport javax.swing.JFrame;
  jimport javax.swing.JLabel;

  url = URL.new("http://www.scilab.org/var/ezwebin_site/storage/images/media/images/puffin/1948-1-eng-GB/puffin.png");
  image = ImageIO.read(url);

  frame = JFrame.new(jvoid);
  icon = ImageIcon.new(image);
  label = JLabel.new(icon);
  pane = frame.getContentPane(jvoid);
  pane.add(label, BorderLayout.CENTER);
  frame.pack(jvoid);
  frame.setVisible(%T);

  jremove frame icon label pane url image BorderLayout URL ImageIO ImageIcon JFrame JLabel
  jautoUnwrap(%F);

endfunction

demo_swing();
clear demo_swing;