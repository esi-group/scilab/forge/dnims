/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "getSciArgs.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_getmethods(char *fname)
{
    SciErr err;
    int *addr = NULL;
    int row = 0, col = 0;
    int *id = NULL;
    char *errmsg = NULL;

    CheckRhs(1, 1);

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    if (!isEnvObj(addr) && !isEnvClass(addr))
    {
        Scierror(999, "%s: Wrong type for argument 1 : _EObj or _EClass expected\n", fname);
        return 0;
    }

    err = getMatrixOfInteger32InList(pvApiCtx, addr, 3, &row, &col, &id);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    getaccessiblemethods(fname, *id, Rhs + 1, &errmsg);
    if (errmsg)
    {
        Scierror(999,"%s: %s\n", "getMethods", errmsg);
        FREE(errmsg);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
