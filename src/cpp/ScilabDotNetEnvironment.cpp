/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be usesd under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/* ------------------------------------------------------------------------- */
#include "ScilabDotNetEnvironment.hxx"
#include "ScilabDotNetObjects.h"
/* ------------------------------------------------------------------------- */
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "ScilabObjects.hxx"
#include <string.h>
#include "NoMoreScilabMemoryException.hxx"
#include "wrap.hpp"
#include "wrapwithcast.hpp"
#include "unwrap.hpp"
//#include "WrapAsDirectBufferTemplate.hpp"
#ifdef _MSC_VER
#include "strdup_windows.h"
#endif

using namespace ScilabObjects;
/* ------------------------------------------------------------------------- */
ScilabDotNetEnvironment::ScilabDotNetEnvironment()
{
	ScilabDotNetObjects::Initialize();
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::garbagecollect()
{
	ScilabDotNetObjects::GarbageCollect();
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::createarray(char *className, int* dims, int len)
{
	return ScilabDotNetObjects::CreateArray(className, dims, len);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::loadclass(char *className, char allowReload)
{	
	return ScilabDotNetObjects::LoadClass(className, allowReload);
}
/* ------------------------------------------------------------------------- */
char* ScilabDotNetEnvironment::getrepresentation(int id)
{
   return ScilabDotNetObjects::GetObjectRepresentation(id);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::isvalidobject(int id)
{
	return ScilabDotNetObjects::IsValidDotNetObject(id);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::newinstance(int id, int *args, int argsSize)
{
	return ScilabDotNetObjects::NewInstance(id, args, argsSize);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::invoke(int id, char *methodName, int *args, int argsSize)
{
	return ScilabDotNetObjects::Invoke(id, methodName, args, argsSize);
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::setfield(int id, char *fieldName, int idarg)
{
	ScilabDotNetObjects::SetField(id, fieldName, idarg);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::getfield(int id, char *fieldName)
{
	return ScilabDotNetObjects::GetField(id, fieldName);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::getfieldtype(int id, char *fieldName)
{
	return ScilabDotNetObjects::GetFieldType(id, fieldName);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::getarrayelement(int id, int *index, int length)
{
	return ScilabDotNetObjects::GetArrayElement(id, index, length);
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::setarrayelement(int id, int *index, int length, int idArg)
{
	ScilabDotNetObjects::SetArrayElement(id, index, length, idArg);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::cast(int id, char *objName)
{
	return ScilabDotNetObjects::Cast(id, objName);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::castwithid(int id, int classId)
{
	return ScilabDotNetObjects::CastWithId(id, classId);
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::removescilabobject(int id)
{
	ScilabDotNetObjects::RemoveDotNetObject(id);
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::getaccessiblemethods(int id, int pos)
{
	//TO-DO: get strings array and created Scilab matrix
	int len = 0;
	char** methodNames = ScilabDotNetObjects::GetAccessibleMethods(id, len);

	int lenCol = len == 0 ? 0 : 1;
	SciErr err = createMatrixOfString(pvApiCtx, pos, lenCol, len, methodNames);
	if (err.iErr)
	{
		throw ScilabObjects::NoMoreScilabMemoryException();
	}	
}
/* ------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::getaccessiblefields(int id, int pos)
{
	int len = 0;
	char** fieldNames = ScilabDotNetObjects::GetAccessibleFields(id, len);

	int lenCol = len == 0 ? 0 : 1;
	SciErr err = createMatrixOfString(pvApiCtx, pos, lenCol, len, fieldNames);
	if (err.iErr)
	{
		throw ScilabObjects::NoMoreScilabMemoryException();
	}	
}
/* ------------------------------------------------------------------------- */
char* ScilabDotNetEnvironment::getclassname(int id)
{
	return ScilabDotNetObjects::GetDotNetClassName(id);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::isunwrappable(int id)
{
	return ScilabDotNetObjects::IsUnwrappable(id);
}
/* ------------------------------------------------------------------------- */
int ScilabDotNetEnvironment::compilecode(char *className, char **code, int size)
{
	return ScilabDotNetObjects::CompileCode(className, code, size);
}
/*-------------------------------------------------------------------------- */
//.NET-specific methods
/*-------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::load(const char* longName, bool allowReload)
{
	return ScilabDotNetObjects::Load(longName, allowReload);
}
/*-------------------------------------------------------------------------- */
void ScilabDotNetEnvironment::loadfrom(const char* path, bool allowReload)
{
	return ScilabDotNetObjects::LoadFrom(path, allowReload);
}
