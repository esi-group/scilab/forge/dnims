/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "stack-c.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "api_scilab.h"
#include "noMoreMemory.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/* Function invoke is called with more than 2 arguments : invoke(obj,method,arg1,...,argn)
   - obj is a _JObj mlist so we get his id or can be a String or a number. In this last case, the obj is converted
   in a Java object and the id is got.
   - method is the name of the method.
   - arg1,...,argn are the arguments of the called method, if they're not _JObj mlist, they're converted when it is possible
*/
int sci_invoke(char *fname)
{
    SciErr err;
    int *tmpvar = NULL;
    int *addr = NULL;
    int idObj = 0;
    int *args = NULL;
    int i = 0;
    char *methName = NULL;
    char *errmsg = NULL;
    int ret = 0;

    setCopyOccured(0);
    initialization(fname);
    setIsNew(0);

    if (Rhs < 2)
    {
        Scierror(999, "%s: Wrong number of arguments : more than 2 arguments expected\n", fname);
        return 0;
    }

    tmpvar = (int*)MALLOC(sizeof(int) * (Rhs - 1));
    if (!tmpvar)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    *tmpvar = 0;

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        FREE(tmpvar);
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        FREE(tmpvar);
        return 0;
    }

    args = (int*)MALLOC(sizeof(int) * (Rhs - 2));
    if (!args)
    {
        FREE(tmpvar);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    for (; i < Rhs - 2; i++)
    {
        err = getVarAddressFromPosition(pvApiCtx, i + 3, &addr);
        if (err.iErr)
        {
            FREE(args);
            removeTemporaryVars(fname, tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }
        args[i] = getIdOfArg(addr, fname, tmpvar, 0, i + 3);
        // If args[i] == -1 then we have a scilab variable which cannot be converted in a Java object.
        if (args[i] == - 1)
        {
            FREE(args);
            removeTemporaryVars(fname, tmpvar);
            FREE(tmpvar);
            return 0;
        }
    }

    methName = getSingleString(2, fname);
    if (!methName)
    {
        removeTemporaryVars(fname, tmpvar);
        FREE(tmpvar);
        FREE(args);
        return 0;
    }

    ret = invoke(fname, idObj, methName, args, Rhs - 2, &errmsg);
    freeAllocatedSingleString(methName);
    FREE(args);

    removeTemporaryVars(fname, tmpvar);
    FREE(tmpvar);

    if (errmsg)
    {
        Scierror(999, ENVIRONMENTERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }


    if (getAutoUnwrap())
    {
        if (!unwrap(ret, Rhs + 1, fname))
        {
            if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
            {
                removescilabobject(fname, ret);
                return 0;
            }
        }
        else
        {
            removescilabobject(fname, ret);
        }
    }
    else if (!createEnvironmentObjectAtPos(_ENVOBJ, Rhs + 1, ret, fname))
    {
        removescilabobject(fname, ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
