/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <windows.h>
/*--------------------------------------------------------------------------*/
#pragma comment(lib,"d:/Projects/Scilab/GIT-scilab-master/scilab/scilab/modules/JIMS/src/cpp/libjims_cpp.lib")
#pragma comment(lib,"d:/Projects/Scilab/GIT-scilab-master/scilab/scilab/modules/JIMS/src/c/libjims_c.lib")
//#pragma comment(lib,"../../src/cpp/libjims_cpp.lib")
//#pragma comment(lib,"../../src/c/libjims_c.lib")
/*--------------------------------------------------------------------------*/
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
  switch (reason)
    {
    case DLL_PROCESS_ATTACH:
      break;
    case DLL_PROCESS_DETACH:
      break;
    case DLL_THREAD_ATTACH:
      break;
    case DLL_THREAD_DETACH:
      break;
    }
  return 1;
}
/*--------------------------------------------------------------------------*/

