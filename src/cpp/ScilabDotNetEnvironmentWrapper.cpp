#include "ScilabDotNetEnvironmentWrapper.hxx"
#include "ScilabAbstractEnvironmentException.hxx"
#include "ScilabDotNetObjects.h"
#include "api_scilab.h"
#include "OptionsHelper.h"

#define ScilabDotNetEnvironmentWrap(Type, type) int ScilabDotNetEnvironmentWrapper::wrap##Type(type x) \
	{ \
		return ScilabDotNetObjects::wrap##Type(x); \
	} \
	int ScilabDotNetEnvironmentWrapper::wrap##Type(type* x, int xSize) \
	{ \
		return ScilabDotNetObjects::wrap##Type(x, xSize); \
	} \
	int ScilabDotNetEnvironmentWrapper::wrap##Type(type** x, int xSize, int xColSize) \
	{ \
		return ScilabDotNetObjects::wrap##Type(x, xSize, xColSize); \
	}

#define ScilabDotNetEnvironmentUnwrap(Type, type, ScilabType, typeName) void ScilabDotNetEnvironmentWrapper::unwrap##typeName(int id, int pos) \
		{ \
			SciErr err; \
			type addr = ScilabDotNetObjects::unwrap##Type(id); ; \
			err = createMatrixOf##ScilabType(pvApiCtx, pos, 1, 1, &addr); \
			if (err.iErr) \
			{ \
				throw ScilabAbstractEnvironmentException("Allocation fail");\
			} \
		} \
		void ScilabDotNetEnvironmentWrapper::unwraprow##typeName(int id, int pos) \
		{ \
			type* addr = NULL; \
			SciErr err; \
			int len = 0; \
			addr = ScilabDotNetObjects::unwrapRow##Type(id, len); \
			err = createMatrixOf##ScilabType(pvApiCtx, pos, 1, len, addr); \
			if (err.iErr) \
			{ \
				throw ScilabAbstractEnvironmentException("Allocation fail"); \
			} \
		} \
		void ScilabDotNetEnvironmentWrapper::unwrapmat##typeName(int id, int pos) \
		{ \
			type* addr = NULL; \
			SciErr err; \
			int lenCol = 0, lenRow = 0; \
			addr = ScilabDotNetObjects::unwrapMat##Type(id, lenCol, lenRow, getMethodOfConv()); \
			err = createMatrixOf##ScilabType(pvApiCtx, pos, lenRow, lenCol, addr); \
			if (err.iErr) \
			{ \
				throw ScilabAbstractEnvironmentException("Allocation fail"); \
			} \
		}

ScilabDotNetEnvironmentWrap(Double, double)
ScilabDotNetEnvironmentWrap(Int, int)
ScilabDotNetEnvironmentWrap(Byte, byte)
ScilabDotNetEnvironmentWrap(Short, short)
ScilabDotNetEnvironmentWrap(Char, unsigned short)
ScilabDotNetEnvironmentWrap(UInt, long long)
ScilabDotNetEnvironmentWrap(UByte, short)
ScilabDotNetEnvironmentWrap(UShort, int)
ScilabDotNetEnvironmentWrap(Boolean, bool)
ScilabDotNetEnvironmentWrap(Float, float)
ScilabDotNetEnvironmentWrap(Long, long long)
ScilabDotNetEnvironmentWrap(String, char*)

ScilabDotNetEnvironmentUnwrap(Double,double,Double,double)
ScilabDotNetEnvironmentUnwrap(Boolean,int,Boolean,boolean)
ScilabDotNetEnvironmentUnwrap(Int,int,Integer32,int)
ScilabDotNetEnvironmentUnwrap(Byte,char,Integer8,byte)
ScilabDotNetEnvironmentUnwrap(Short,short,Integer16, short)
ScilabDotNetEnvironmentUnwrap(Char,unsigned short,UnsignedInteger16,char)
ScilabDotNetEnvironmentUnwrap(Double,double,Double,float)
ScilabDotNetEnvironmentUnwrap(String,char*,String,string)
#ifdef __SCILAB_INT64__
ScilabDotNetEnvironmentUnwrap(Long,long,Integer64,long)
#else
ScilabDotNetEnvironmentUnwrap(Long,unsigned int,UnsignedInteger32,long)
#endif
